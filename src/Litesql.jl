module Litesql

"""
Litesql is a Julia interface to the [Sqlite library](http://www.sqlite.org)
===========================================================================
"""
Litesql

# The system or locally compiled SQLite so/dll is loaded with Libdl (no artifact) 
using Libdl,Serialization

export DB,AutocloseDB,SqliteDB,SqliteError,SqliteBusy,SqliteSizeError,StmtError,Blob
export AbstractStmt,SqlStmt,AutoStmt,Statement
export 
    SQLITE_OK,SQLITE_DONE,SQLITE_BUSY,SQLITE_LOCKED,SQLITE_ROW,
    SQLITE_OPEN_READONLY, SQLITE_OPEN_READWRITE, SQLITE_OPEN_CREATE,
    SQLITE_INTEGER,SQLITE_FLOAT,SQLITE_BLOB,SQLITE_NULL,SQLITE_TEXT
export
    opendb,attach,detach,databases,errmsg,
    db_filename,db_status,readonly,close,close!,finalize_close,finalize_close!,isclosed,
    extended_result_codes,extended_errcode,
    exec,checked_exec,run,transaction,commit,savepoint,release,rollback,list,listschema,
    tables,views,vtables,table_info,table_column_metadata,
    delete,insert,update,transact,select,select2tuples,select2vecs,select!,collect,count,
    busy_timeout,enable_load_extension,load_extension,limit,wal_checkpoint,
    busy,step,reset,finalize_stmt,finalize!,list_unfinalized,checked_reset,checked_step,
    row,rows,rowvec,rowtuple,eachrow,eachrowvec,eachrowtuple,eachcol,eachcolnm,channel,split,
    sql_insert,sql_update,sql_select,complete,
    prepare_select,prepare_insert,prepare_update,prepare_delete,prepare,explain_stmt!,explain,
    db_handle,db_filename,schema,sql,expanded_sql,prepend,
    malloc,realloc,free,msize,db_release_memory,
    bind_parameter_count,bind_parameter_name,bind_parameter_index,clear_bindings,
    bind,bind_zeroblob,checked_bind,firststep,
    last_insert_rowid,ntuple,
    columns,ncols,column,eachkey,nullable,
    column_count,data_count,column_type,column_name,column_decltype, column_names,
    column_double,column_int64,column_uint64,column_int,
    column_text,column_blob,column_bytes,column_value,column_vector,
    column_float,column_float32,column_single,column_string,column_ascii,column_blobcopy,
    column_doubleblob,column_float64,column_object,column_null,
    ResCols,@c_str,Rowit,Rowith,Rowiths

import Base: close,isopen,show,throw,detach,checkbounds
import Base: bind,first,step,reset,collect,ntuple
import Base: iterate,length,eltype,eachrow,eachcol,count,eachindex,split
import Base: IteratorSize,SizeUnknown,HasLength
import Base: getindex,|>,broadcast
import Base: run,print,write,unsafe_read,unsafe_write
import Distributed: interrupt

"""
    `SINGLE_IS_BLOB`

    allows to write IEEE 754 32-bit floats into an SQLite database and faithfully read them back,
    significantly saving disk space for large amounts of single precision float data.

    Set `Litesql.SINGLE_IS_BLOB[]=true` to insert/retrieve `Float32` values as 4-byte BLOBs.
    My [SQLite `vector` extension](https://gitlab.com/stephancb/sqlite-vector-extension) can be used
    to work within SQL with 4-byte BLOBS representing 32-bit floats.

    The flag should be used consistently, i.e. both read and write operations
    on the database must be done with the same false/true value of `Litesql.SINGLE_IS_BLOB[]`.

    See also [`bind`](@ref)`(..., ::Float32)`, [`column_float32`](@ref),
             [vector extension](https://gitlab.com/stephancb/sqlite-vector-extension) for SQLite
"""
const SINGLE_IS_BLOB = Ref(false)
"""
    `NAN_IS_BLOB`

    allows to write IEEE 754 `NaN`s into an SQLite database and faithfully read them back.

    Set `Litesql.NAN_IS_BLOB[]=true` to insert/retrieve `NaN`s as 8/4-byte BLOBs. SQL functions/expressions
    do not correctly work with BLOB `NaN`s (instead of NULLs).

    The caller must apply this flag consistently, i.e. both read and write operations
    on the database need to use the same false/true value of `Litesql.NAN_IS_BLOB[]`.

    See also [`bind`](@ref)`(..., ::Float64)`, [`column_float`](@ref),
             [vector extension](https://gitlab.com/stephancb/sqlite-vector-extension) for SQLite
"""
const NAN_IS_BLOB = Ref(false)
"""
    `DOUBLE_IS_BLOB`

    allows to write IEEE 754 64-bit floats, incl. `NaN`s, into a SQLite database and read them back.

    Set `Litesql.DOUBLE_IS_BLOB[]=true` to insert/retrieve C double/64-bit
    float values as 8-byte BLOB.

    My [SQLite `vector` extension](https://gitlab.com/stephancb/sqlite-vector-extension) can be used
    to work within SQL with 8-byte BLOBS representing doubles.

    The flag should be used consistently, i.e. both read and write operations
    on the database must be done with the same false/true value of `Litesql.DOUBLE_IS_BLOB[]`.

    See also [`bind`](@ref)`(..., ::Float64)`, [`column_float`](@ref),
             [vector extension](https://gitlab.com/stephancb/sqlite-vector-extension) for SQLite
"""
const DOUBLE_IS_BLOB = Ref(false)

#Return codes
const SQLITE_OK =            0  # /* Successful result */
const SQLITE_BUSY =          5  # /* The database file is locked */
const SQLITE_LOCKED =        6  # /* A table in the database is locked */
const SQLITE_NOMEM =         7  # /* SQLite was unable to allocate all the memory */
const SQLITE_READONLY =      8  # /* An attempt is made to alter some data without write permission */
const SQLITE_INTERUPT =      9  # /* An operation was interrupted by the sqlite3_interrupt() interface */
const SQLITE_ROW =         100  # /* sqlite3_step() has another row ready */
const SQLITE_DONE =        101  # /* sqlite3_step() has finished executing */

#Flags For File Open Operations
const SQLITE_OPEN_READONLY       = 0x00000001  #/* Use with sqlite3_open_v2() */
const SQLITE_OPEN_READWRITE      = 0x00000002  #/* Use with sqlite3_open_v2() */
const SQLITE_OPEN_CREATE         = 0x00000004  #/* Use with sqlite3_open_v2() */
const SQLITE_OPEN_DELETEONCLOSE  = 0x00000008  #/* VFS only */
const SQLITE_OPEN_EXCLUSIVE      = 0x00000010  #/* VFS only */
const SQLITE_OPEN_AUTOPROXY      = 0x00000020  #/* VFS only */
const SQLITE_OPEN_URI            = 0x00000040  #/* Ok for sqlite3_open_v2() */
const SQLITE_OPEN_MEMORY         = 0x00000080  #/* Ok for sqlite3_open_v2() */
const SQLITE_OPEN_MAIN_DB        = 0x00000100  #/* VFS only */
const SQLITE_OPEN_TEMP_DB        = 0x00000200  #/* VFS only */
const SQLITE_OPEN_TRANSIENT_DB   = 0x00000400  #/* VFS only */
const SQLITE_OPEN_MAIN_JOURNAL   = 0x00000800  #/* VFS only */
const SQLITE_OPEN_TEMP_JOURNAL   = 0x00001000  #/* VFS only */
const SQLITE_OPEN_SUBJOURNAL     = 0x00002000  #/* VFS only */
const SQLITE_OPEN_MASTER_JOURNAL = 0x00004000  #/* VFS only */
const SQLITE_OPEN_NOMUTEX        = 0x00008000  #/* Ok for sqlite3_open_v2() */
const SQLITE_OPEN_FULLMUTEX      = 0x00010000  #/* Ok for sqlite3_open_v2() */
const SQLITE_OPEN_SHAREDCACHE    = 0x00020000  #/* Ok for sqlite3_open_v2() */
const SQLITE_OPEN_PRIVATECACHE   = 0x00040000  #/* Ok for sqlite3_open_v2() */
const SQLITE_OPEN_WAL            = 0x00080000  #/* VFS only */

#Sqlite data types
const SQLITE_INTEGER  = 1
const SQLITE_FLOAT    = 2
const SQLITE_BLOB     = 4
const SQLITE_NULL     = 5
const SQLITE_TEXT     = 3

# Text Encodings
const SQLITE_UTF8     = 1    #/* IMP: R-37514-35566 */

#Destructor behaviour
const SQLITE_STATIC =       0   # /* Destructor not needed */
const SQLITE_TRANSIENT =   -1   # /* Will be destroyed, make a copy */

#EXPLAIN
const EXPLAIN=1
const EXPLAIN_QUERY_PLAN=2

"""
    `destr_static`

    destructor constant for `bind(...` functions binding values to SQLite statement parameters,
    assumes that no destructor actions is need.

    See also `Litesql.destr_transient`
"""
const destr_static=Ptr{Nothing}(Litesql.SQLITE_STATIC)
"""
    `destr_transient`

    destructor constant for `bind(...` functions binding values to SQLite statement parameters,
    makes a copy of the string/array before returning.

    See also `Litesql.destr_static`
"""
const destr_transient=Ptr{Nothing}(Litesql.SQLITE_TRANSIENT)

# Modes for WAL checkpoints
const SQLITE_CHECKPOINT_PASSIVE  = 0  #/* Do as much as possible w/o blocking */
const SQLITE_CHECKPOINT_FULL     = 1  #/* Wait for writers, then checkpoint */
const SQLITE_CHECKPOINT_RESTART  = 2  #/* Like FULL but wait for for readers */
const SQLITE_CHECKPOINT_TRUNCATE = 3  #/* Like RESTART but also truncate WAL */

# run-time status parameters that can be returned by sqlite3_status().
const SQLITE_STATUS_MEMORY_USED        = 0
const SQLITE_STATUS_PAGECACHE_USED     = 1
const SQLITE_STATUS_PAGECACHE_OVERFLOW = 2
#define SQLITE_STATUS_SCRATCH_USED       3  /* NOT USED */
#define SQLITE_STATUS_SCRATCH_OVERFLOW   4  /* NOT USED */
const SQLITE_STATUS_MALLOC_SIZE        = 5
const SQLITE_STATUS_PARSER_STACK       = 6
const SQLITE_STATUS_PAGECACHE_SIZE     = 7
#define SQLITE_STATUS_SCRATCH_SIZE       8  /* NOT USED */
const SQLITE_STATUS_MALLOC_COUNT       = 9

# Status Parameters for database connections
const SQLITE_DBSTATUS_LOOKASIDE_USED      =  0
const SQLITE_DBSTATUS_CACHE_USED          =  1
const SQLITE_DBSTATUS_SCHEMA_USED         =  2
const SQLITE_DBSTATUS_STMT_USED           =  3
const SQLITE_DBSTATUS_LOOKASIDE_HIT       =  4
const SQLITE_DBSTATUS_LOOKASIDE_MISS_SIZE =  5
const SQLITE_DBSTATUS_LOOKASIDE_MISS_FULL =  6
const SQLITE_DBSTATUS_CACHE_HIT           =  7
const SQLITE_DBSTATUS_CACHE_MISS          =  8
const SQLITE_DBSTATUS_CACHE_WRITE         =  9
const SQLITE_DBSTATUS_DEFERRED_FKS        = 10
const SQLITE_DBSTATUS_CACHE_USED_SHARED   = 11
const SQLITE_DBSTATUS_CACHE_SPILL         = 12
const SQLITE_DBSTATUS_MAX                 = 12   # /* Largest defined DBSTATUS */

# Status Parameters for prepared statements
const SQLITE_STMTSTATUS_FULLSCAN_STEP = 1
const SQLITE_STMTSTATUS_SORT          = 2
const SQLITE_STMTSTATUS_AUTOINDEX     = 3
const SQLITE_STMTSTATUS_VM_STEP       = 4
const SQLITE_STMTSTATUS_REPREPARE     = 5
const SQLITE_STMTSTATUS_RUN           = 6
const SQLITE_STMTSTATUS_MEMUSED       = 99

# Library config options
const SQLITE_CONFIG_SINGLETHREAD   =  1 # /* nil */
const SQLITE_CONFIG_MULTITHREAD    =  2 # /* nil */
const SQLITE_CONFIG_SERIALIZED     =  3 # /* nil */
const SQLITE_CONFIG_MALLOC         =  4 # /* sqlite3_mem_methods* */
const SQLITE_CONFIG_GETMALLOC      =  5 # /* sqlite3_mem_methods* */
const SQLITE_CONFIG_SCRATCH        =  6 # /* No longer used */
const SQLITE_CONFIG_PAGECACHE      =  7 # /* void*, int sz, int N */
const SQLITE_CONFIG_HEAP           =  8 # /* void*, int nByte, int min */
const SQLITE_CONFIG_MEMSTATUS      =  9 # /* boolean */
const SQLITE_CONFIG_MUTEX          = 10 # /* sqlite3_mutex_methods* */
const SQLITE_CONFIG_GETMUTEX       = 11 # /* sqlite3_mutex_methods* */
# /* previously SQLITE_CONFIG_CHUNKALLOC 12 which is now unused. */
const SQLITE_CONFIG_LOOKASIDE      =  13 # /* int int */
const SQLITE_CONFIG_PCACHE         =  14 # /* no-op */
const SQLITE_CONFIG_GETPCACHE      =  15 # /* no-op */
const SQLITE_CONFIG_LOG            =  16 # /* xFunc, void* */
const SQLITE_CONFIG_URI            =  17 # /* int */
const SQLITE_CONFIG_PCACHE2        =  18 # /* sqlite3_pcache_methods2* */
const SQLITE_CONFIG_GETPCACHE2     =  19 # /* sqlite3_pcache_methods2* */
const SQLITE_CONFIG_COVERING_INDEX_SCAN = 20 # /* int */
const SQLITE_CONFIG_SQLLOG         = 21 # /* xSqllog, void* */
const SQLITE_CONFIG_MMAP_SIZE      = 22 # /* sqlite3_int64, sqlite3_int64 */
const SQLITE_CONFIG_WIN32_HEAPSIZE = 23 # /* int nByte */
const SQLITE_CONFIG_PCACHE_HDRSZ   = 24 # /* int *psz */
const SQLITE_CONFIG_PMASZ          = 25 # /* unsigned int szPma */
const SQLITE_CONFIG_STMTJRNL_SPILL = 26 # /* int nByte */
const SQLITE_CONFIG_SMALL_MALLOC   = 27 # /* boolean */
const SQLITE_CONFIG_SORTERREF_SIZE = 28 # /* int nByte */
const SQLITE_CONFIG_MEMDB_MAXSIZE  = 29 # /* sqlite3_int64 */

# DB config
const SQLITE_DBCONFIG_ENABLE_LOAD_EXTENSION = 1005

#Run-time limit categories
const SQLITE_LIMIT_LENGTH                  = 0
const SQLITE_LIMIT_SQL_LENGTH              = 1
const SQLITE_LIMIT_COLUMN                  = 2
const SQLITE_LIMIT_EXPR_DEPTH              = 3
const SQLITE_LIMIT_COMPOUND_SELECT         = 4
const SQLITE_LIMIT_VDBE_OP                 = 5
const SQLITE_LIMIT_FUNCTION_ARG            = 6
const SQLITE_LIMIT_ATTACHED                = 7
const SQLITE_LIMIT_LIKE_PATTERN_LENGTH     = 8
const SQLITE_LIMIT_VARIABLE_NUMBER         = 9
const SQLITE_LIMIT_TRIGGER_DEPTH           =10
const SQLITE_LIMIT_WORKER_THREADS          =11

#Prepare flags for v3
const SQLITE_PREPARE_PERSISTENT = 0x00000001
const SQLITE_PREPARE_NORMALIZE  = 0x00000002
const SQLITE_PREPARE_NO_VTAB    = 0x00000004

# SQL Trace Event Codes
const  SQLITE_TRACE_STMT     = 0x01
const  SQLITE_TRACE_PROFILE  = 0x02
const  SQLITE_TRACE_ROW      = 0x04
const  SQLITE_TRACE_CLOSE    = 0x08

# function __init__()
#     @require ZipFile="a5390f91-8eb1-5f08-bee0-b1d1ffed6cea" include("zippedtrace.jl")
#     @require Tables="bd369af6-aec1-5ad0-b16a-f7cc5008161c" include("tables.jl")
# end

if Sys.isunix()
    # prefer to use a locally compiled sqlite libraries
    # const slb = Libdl.find_library("libsqlite3", ["/usr/local/lib"])
    const slb = Libdl.find_library("libsqlite3")
    # const slp = Libdl.dlopen("libsqlite3")
elseif Sys.iswindows()
    # const slb = joinpath( realpath(dirname(@__FILE__)), "sqlite3.dll" )
    const slb = Libdl.find_library("sqlite3")
else
    error("Cannot find the Sqlite library")
end

"""
    ResCols(s)

    is a non-standard String type presenting result columns,
    i.e. a comma separated list of column names or SQL strings
"""
struct ResCols
    str::String
end

macro c_str(s)
    ResCols(s)
end

"""
    rex_rescols

is a regex matching commas, except those within SQL strings (within quotes ')
"""
const rex_rescols = r",(?=([^']*'[^']*')*[^']*$)"

"""
    split(::ResCols) ->

    splits a `ResCols` string, i.e a comma separated list, into a vector of column names.
    Commas inside SQL strings (in quotes '...' do not split.) 
"""
split(s::ResCols) = split(s.str, rex_rescols) 

"""
    ncols(x) -> returns the number of columns listed in 'x' (usually a String)
                computed as the nr of occurences of ',',
                except those within single quotes (SQL strings), plus one.
"""
ncols(x::AbstractString) = count(!isempty, eachmatch(rex_rescols, x))+1
# ncols(x::AbstractString) = count(!isempty, eachmatch(r"(,)(?=(?:[^']|'[^']*')*$)", x))+1
# ncols(x::AbstractStmt) = count(',', x)+1

# Abbreviate a little the C calls: use like ccall(@s3 funcnm, ...)
macro s3(funcnm)
    (Symbol("sqlite3_", string(funcnm)), slb)
end

# My extensionfunctions are in:
const EXTPATH = Ref(joinpath(homedir(), ".sqlite/extensions"))

# default extensions to load when opening
"""
DEFEXTENSIONS is a reference to a vector of SQLite extensions that are preloaded with
`opendb(filename, mode::String, ... )`
"""
const DEFEXTENSIONS = Ref(["decimal","fileio","ieee754","series","zipfile"])

# functions that directly address the Sqlite library (i.e. not a database connection)

# SQLite version
sourceid() = unsafe_string(ccall((@s3 sourceid), Cstring, ()))
libversion() = unsafe_string(ccall((@s3 libversion), Cstring, ()))
jlversion() = parse(VersionNumber, libversion())
libversion_number() = ccall((@s3 libversion_number), Cint, ())

# Hooks into the Sqlite memory allocation
malloc(nbytes) = ccall((@s3 malloc), Ptr{Cvoid}, (Cint,), nbytes)
malloc(nbytes::Union{UInt64,Int64})  = ccall((@s3 malloc64), Ptr{Cvoid}, (UInt64,), nbytes)
realloc(ptr, nbytes) = ccall((@s3 realloc), Ptr{Cvoid}, (Ptr{Cvoid}, Cint), ptr, nbytes)
realloc(ptr, nbytes::Union{UInt64,Int64}) =
ccall((@s3 realloc64), Ptr{Cvoid}, (Ptr{Cvoid}, Int64), ptr, nbytes)
free(ptr)  = ccall((@s3 free), Cvoid, (Ptr{Cvoid},), ptr)
msize(ptr) = ccall((@s3 msize), UInt64, (Ptr{Cvoid},), ptr)

release_memory(nbytes) = ccall((@s3 release_memory), Cint, (Cint,), nbytes)

# Compile time options (will not work if SQLITE_OMIT_COMPILEOPTION_DIAGS=1 was
# used when compiling Sqlite, then comment these functions if Litesql is permanently
# used with such a Sqlite library)
compileoption_used(nm::String) = ccall((@s3 compileoption_used), Cint, (Cstring,), nm)
compileoption_get(n) = ccall((@s3 compileoption_get), Cstring, (Cint,), n)

"""
    `DB` is an abstract parent to either static (SqliteDB) or automatically closing (AutocloseDB)
    Julia objects having a handle to Sqlite databases.

    `prepare` and many other functions take a `DB` as input argument. 
"""
abstract type DB end

"""
    SqliteDB

    has the handle of an Sqlite database connection. `opendb` can return an SqliteDB.
    The connection is closed with `close`.
"""
struct SqliteDB <: DB
    handle::Ptr{Cvoid}
end

function show(io::IO, db::DB)
    if db.handle == C_NULL
        print(io, "DB: not connected")
    else
        fnm = db_filename(db)
        if isempty(fnm)
            fnm = ":memory:"
        end
        print(io, "DB: $fnm; readonly: $(readonly(db))")
    end
end

"""
    AutocloseDB

    has the handle of an Sqlite database connection. `opendb` returns an AutocloseDB by default.
    The connection automatically closes when out of scope. `close!` forces closing. 
"""
mutable struct AutocloseDB <: DB
    handle::Ptr{Cvoid}
    # AutocloseDB(h) = finalizer(finalize_close!, new(h))
    AutocloseDB(h) = finalizer(finalize_close!, new(h))
end
isclosed(db) = db.handle==C_NULL
isopen(db)   = db.handle!=C_NULL

"""
    SqliteError(msg, txt) -> sqlite_exception

constructs an exception object for errors reported by the SQLite library.
Usually `msg` is the SQLite error string, and `txt` has some info about how the
error occured in Litesql. The object is shown as
"txt
Sqlite says: msg"
"""
mutable struct SqliteError <: Exception
    msg::String   # Sqlite error message
    txt::String   # SQL statement
    # function SqliteError(msg, txt)
    #     println("SqliteError Exception:\n   msg = $msg\n   txt = $txt")
    #     new(msg, txt)
    # end
end
SqliteError(msg) = SqliteError(msg, "")
function show(io::IO, err::SqliteError)
    if !isempty(err.txt)
        print(io, "$(err.txt)\n")
    end
    print(io, "Sqlite says: $(err.msg)")
end

struct SqliteBusy <: Exception; end
show(io::IO, ::SqliteBusy) = print(io, "Sqlite: busy")

struct SqliteSizeError <: Exception; end
show(io::IO, ::SqliteSizeError) = print(io, "Sqlite: size mismatch Blob<->Vector")

function _opendb(filename, flags=SQLITE_OPEN_READONLY, vfs=C_NULL)
    handlep = Ref(C_NULL) 
    fnm     = expanduser(filename)
    iret = ccall((@s3 open_v2), Cint,
                 (Cstring, Ptr{Cvoid}, Cint,  Ptr{UInt8}),
                 fnm     , handlep,    flags, vfs)
    if iret!=SQLITE_OK
        throw( SqliteError(errstr(iret), ": $fnm") )
        println("error was thrown!")
    end
    return handlep[]
end

"""
    opendb(filename, flags=SQLITE_OPEN_READONLY, vfs::Ptr{Nothing}=0) -> db

    opens a database file and returns a DB object with the handle to the database.
    The Sqlite function [open_v2] (https://www.sqlite.org/c3ref/open.html) is called.
    `SQLITE_OPEN_READWRITE` and `SQLITE_OPEN_CREATE` are alternative frequently used flags.
    An automatically closing AutocloseDB is returned.
"""
opendb(filename, flags=SQLITE_OPEN_READONLY | SQLITE_OPEN_FULLMUTEX, vfs=C_NULL) =
    AutocloseDB(_opendb(filename, flags, vfs))

"""
    opendb(Val(:noauto), filename, flags=SQLITE_OPEN_READONLY, vfs::Ptr{Nothing}=0)

    is like opendb(filename, flags, vfs) but returns an SqliteDB which needs to be closed
    explicitely with the `close` function.
"""
opendb(::Val{:noauto}, filename, flags=SQLITE_OPEN_READONLY,
       vfs=C_NULL) = SqliteDB(_opendb(filename, flags, vfs))

"""
    opendb(filename, mode::String, ::Ptr{Nothing}=0;
           extensions=DEFEXTENSIONS, attachdbs=(), autoclose=true)

    `mode` can contain
        `r` for read only
        `w` for read and write
        `c` for create if not exists
        `e` for enabling loading of extensions and loading those listed by the keyword `extensions`
        `a` for attaching databases listed in keyword variable `attachdbs`
        `m` for multi-thread mode
        `f` for serialized, no restrictions for multiple threads
"""
function opendb(filename::AbstractString, mode::AbstractString, vfs=C_NULL;
        extensions=DEFEXTENSIONS[], attachdbs=(), timeout=30000, autoclose=true)
    f = SQLITE_OPEN_READONLY
    if occursin('w', mode)
        f = SQLITE_OPEN_READWRITE
    end
    if occursin('c', mode)
        f = SQLITE_OPEN_CREATE | SQLITE_OPEN_READWRITE
    end
    if occursin('r', mode)
        f = SQLITE_OPEN_READONLY
    end
    occursin('m', mode) && (f |= SQLITE_OPEN_NOMUTEX)
    occursin('f', mode) && (f |= SQLITE_OPEN_FULLMUTEX)
    if autoclose
        db = opendb(filename, f, vfs)
    else
        db = opendb(Val(:noauto), filename, f, vfs)
    end
    if occursin('e', mode)
        enable_load_extension(db, true)
        foreach(ext -> load_extension(db, ext), extensions)
    end
    if occursin('a', mode)
        foreach(att -> attach(db, att), attachdbs)
    end
    if occursin('b', mode)
        busy_timeout(db, timeout)
    end
    db
end

"""
    opendb(filename, SqliteDB, flags=SQLITE_OPEN_READONLY, vfs::Ptr{Nothing}=0)

    is like opendb(filename, flags, vfs) but returns an SqliteDB which is not automatically closing.
"""
opendb(filename, ::Type{SqliteDB}, flags=SQLITE_OPEN_READONLY, vfs=C_NULL) =
    SqliteDB(_opendb(filename, flags, vfs))

"""
    opendb(; mode="w", kwargs...) opens a database in memory.
"""
opendb(; mode="w", kwargs...) = opendb(":memory:", mode; kwargs...)

"""
    opendb(...) do db
        ...
    end
    
    works with the usual `do` block having local stop and automatic closing the connection. 
"""
function opendb(func::Function, args...; kwargs...)
    db = opendb(args...; kwargs...)
    func(db)
end

"""
    close(::SqliteDB) -> closed SqliteDB

    closes an SQLite database. Default opened databases are closed automatically.
    This function is only needed for closing non-default opened databases. In this case
    closing a second time is likely to cause a segfault.
"""
function close(db::SqliteDB)
    if db.handle===nothing || db.handle==C_NULL
        ArgumentError("trying to close NULL handle?")
    end
    close_v2(db.handle)!=SQLITE_OK && throw( SqliteError(errmsg(db), "when closing $(db_filename(db)))") )
end

"""
    close!(::AutocloseDB) -> closed AutocloseDB

    force closing a SQLite database before autoclosing kicks in.
"""
function close!(db::AutocloseDB)
    if close_v2(db.handle)!=SQLITE_OK
        throw( SqliteError(errmsg(db), "when closing $(db_filename(db)))") )
    end
    db.handle = C_NULL
    return db
end

close(h::Ptr{Cvoid}) = ccall((@s3 close), Cint, (Ptr{Cvoid},), h)
close_v2(h::Ptr{Cvoid}) = ccall((@s3 close_v2), Cint, (Ptr{Cvoid},), h)

function db_enable_load_extension(db::DB, api_only=1, state=false)
    statep = state ? Ref(0) : C_NULL
    iret = ccall((@s3 db_config), Cint, (Ptr{Cvoid}, Cint, Cint, Ptr{Cint}),
                 db.handle, SQLITE_DBCONFIG_ENABLE_LOAD_EXTENSION, api_only, statep)
    iret, state ? statep[] : -1
end

threadsafe() = Bool(ccall((@s3 threadsafe), Cint, ()))

function config(what::Int, args...)
    if what in (SQLITE_CONFIG_SINGLETHREAD,SQLITE_CONFIG_MULTITHREAD,SQLITE_CONFIG_SERIALIZED)
        iret = ccall((@s3 config), Cint, (Cint,), what)
    end
end

errmsg(db) = errmsg(db.handle)
errmsg(p::Ptr) = unsafe_string(ccall((@s3 errmsg), Cstring, (Ptr{Cvoid},), p))
errstr(iret) = unsafe_string(ccall((@s3 errstr), Ptr{UInt8}, (Cint,), iret))

system_errno(db) = ccall((@s3 system_errno), Cint, (Ptr{Cvoid},), db.handle)

readonly(db, dbName="main") =
Bool(ccall((@s3 db_readonly), Cint, (Ptr{Cvoid}, Cstring), db.handle, dbName))

interrupt(db) = ccall((@s3 interrupt), Ptr{Cvoid}, (Ptr{Cvoid},), db.handle)

busy_timeout(db, msec) =
ccall((@s3 busy_timeout), Cint, (Ptr{Cvoid}, Cint), db.handle, Cint(msec))
sleep(msec) = ccall((@s3 sleep), Cint, (Cint,), Cint(msec))

thread_safe() = Bool(ccall((@s3 threadsafe), Cint, ()))

soft_heap_limit(sz::Int) = ccall((@s3 soft_heap_limit), Int64, (Int64,), sz)

db_release_memory(db) = ccall((@s3 db_release_memory), Cvoid, (Ptr{Cvoid},), db.handle)

last_insert_rowid(db) =
ccall((@s3 last_insert_rowid), Clong, (Ptr{Cvoid},), db.handle)

limit(db, limit_category, newval) =
ccall((@s3 limit), Cint, (Ptr{Any}, Cint, Cint), db.handle,
      Cint(limit_category), Cint(newval))

enable_load_extension(db, onoff=true) =
ccall((@s3 enable_load_extension), Cint, (Ptr{Cvoid}, Cint), db.handle, Cint(onoff))

function load_extension(db, libfnm, entrypt=""; extpath=EXTPATH[])
    # errmsgp = Ptr{Ptr{UInt8}}()
    errmsgp = Ref{Ptr{UInt8}}()
    iret = ccall((@s3 load_extension), Cint,
                 (Ptr{Cvoid}, Ptr{UInt8}, Ptr{Cvoid}, Ptr{Ptr{UInt8}}),
                 db.handle, joinpath(extpath, libfnm),
                 isempty(entrypt) ? C_NULL : entrypt, errmsgp)
    if iret!=SQLITE_OK
        errmsg = unsafe_string(errmsgp[])
        free(errmsgp[])
        throw( SqliteError(errmsg, "loading $libfnm") )
    end
    iret
end

# databases(db::DB) = select(db; sql="PRAGMA database_list")

schema(db::DB, table) =
    text(db; columns="sql", from="sqlite_schema", where="name='$table'")

function listschema(db::DB)
    for attached in databases(db)
        for tbl in vtables(db, attached)
            list(db, columns="sql", from="$attached.sqlite_schema", where="name='$tbl'")
        end
    end
end

listschema(db::DB, table; database="main") =
    list(db, columns="sql", from=database*"."*"sqlite_schema", where="name='$table'")
listschema(dbnm, table) = listschema(opendb(dbnm), table)
listschema(dbnm) = listschema(opendb(dbnm))

function db_filename(db, dbName="main")
    fnm = ccall((@s3 db_filename), Ptr{UInt8},
                (Ptr{Cvoid}, Ptr{UInt8}), db.handle, dbName)
    if fnm==C_NULL
        return nothing
    else
        return unsafe_string(fnm)
    end
end # function filename

function status(op; reset=false)
    current = Ref{Int64}()
    highwat = Ref{Int64}()
    iret = ccall((@s3 status64), Cint, (Cint, Ptr{Int64}, Ptr{Int64}, Cint),
                 op, current, highwat, Cint(reset))
    iret!=SQLITE_OK && throw( SqliteError(errstr(iret), "when getting run time status"))
    current[],highwat[]
end

function db_status(db, op; reset=false)
    current = Ref{Int}()
    highwat = Ref{Int}()
    iret = ccall((@s3 db_status), Cint, (Ptr{Cvoid}, Cint, Ptr{Int}, Ptr{Int}, Cint),
                 db.handle, op, current, highwat, Cint(reset))
    iret!=SQLITE_OK && throw( SqliteError(errstr(iret), "when getting db connection status"))
    current[],highwat[]
end

# tables(db::DB) =
#     collect(String, StmtLen(db; columns="name", from="sqlite_schema", where="type='table'"))
tables(db::DB)  = vcat([tables(db, x) for x in databases(db)]...)
views(db::DB)   = vcat([views(db, x) for x in databases(db)]...)
vtables(db::DB) = vcat([vtables(db, x) for x in databases(db)]...)

tables(dbfnm)  = tables(opendb(dbfnm))
views(dbfnm)   = views(opendb(dbfnm))
vtables(dbfnm) = vtables(opendb(dbfnm))

tables(db::DB, dbnm) =
collect(String, StmtLen(db; columns="name", from="$dbnm.sqlite_schema", where="type='table'"))
views(db::DB, dbnm) =
collect(String, StmtLen(db; columns="name", from="$dbnm.sqlite_schema", where="type='view'"))
vtables(db::DB, dbnm) =
collect(String, StmtLen(db; columns="name", from="$dbnm.sqlite_schema",
                        where="type IN ('view','table')"))

"""
    table_list(db; kwargs...) --> pragma_table_list

    selects from [pragam_table_list](https://www.sqlite.org/pragma.html#pragma_table_list),
    i.e. lists tables and views:
    1. schema: the schema in which the table or view appears (for example "main" or "temp").
    2. name: the name of the table or view.
    3. type: the type of object - one of "table", "view", "shadow" (for shadow tables), or "virtual" for virtual tables.
    4. ncol: the number of columns in the table, including generated columns and hidden columns.
    5. wr: 1 if the table is a WITHOUT ROWID table or 0 if is not.
    6. strict: 1 if the table is a STRICT table or 0 if it is not.

    Additional columns will likely be added in future releases.

    table_list(db, table; kwargs...) --> pragma_table_list('table')

    returns only information about that one table.
"""
function table_list(db::DB, table=""; kwargs...)
    if jlversion()>=v"3.37"
        if isempty(table)
            select(db; from="pragma_table_list", kwargs...)
        else
            (stmt=prepare_select(db; from="pragma_table_list('$table')", kwargs...)) |> step
            row(stmt)
        end
    else
        nothing
    end
end
table_list(dbnm, table=""; kwargs...) = jlversion()>=v"3.37" ? table_list(opendb(dbnm), table; kwargs...) : nothing

"""
    table_info(db, table; kwargs...) --> pragma_table_info('table')

    selects from [pragam_table_info](https://www.sqlite.org/pragma.html#pragma_table_info),
    i.e. returns one row for each normal column in the named table:
    1. name: the name of the column.
    2. type: data type if given, else ''.
    3. notnull: whether or not the column can be NULL.
    4. dflt_value: the default value for the column.
    5. pk: either zero for columns that are not part of the primary key,
           or the 1-based index of the column within the primary key.

    table_info does not returninformation about generated columns or hidden columns. Use table_xinfo to get
    a more complete list of columns that includes generated and hidden columns.
"""
table_info(db::DB, table; kwargs...) = select(db; from="pragma_table_info('$table')", kwargs...)
table_info(dbnm, table; kwargs...) = table_info(opendb(dbnm), table; kwargs...)

"""
    table_xinfo(db, table; kwargs...) --> pragma_table_xinfo('table')

    selects from [pragam_table_xinfo](https://www.sqlite.org/pragma.html#pragma_table_xinfo),
    i.e. returns one row for each column in the named table, including generated columns and hidden columns.
    The output has the same columns as for table_info plus:
    6. hidden: value signifies a normal column (0), a dynamic or stored generated column (2 or 3),
       or a hidden column in a virtual table (1).
"""
table_xinfo(db::DB, table; kwargs...) = select(db; from="pragma_table_xinfo('$table')", kwargs...)
table_xinfo(dbnm, table; kwargs...) = table_xinfo(opendb(dbnm), table; kwargs...)

pragma_table_info(db::DB, table::AbstractString) = select(db, sql="PRAGMA table_info($table)")
pragma_table_info(dbnm::AbstractString, table::AbstractString) =
pragma_table_info(opendb(dbnm), table)

"""
    index_list(db, table; kwargs...) --> pragma_index_list('table')

    selects from [pragam_index_list](https://www.sqlite.org/pragma.html#pragma_index_list),
    i.e. returns one row for each index associated with the given table. Output columns from the index_list are:
    1. A sequence number assigned to each index for internal tracking purposes.
    2. The name of the index.
    3. "1" if the index is UNIQUE and "0" if not.
    4. "c" if the index was created by a CREATE INDEX statement, "u" if the index was created by a UNIQUE constraint,
        or "pk" if the index was created by a PRIMARY KEY constraint.
    5. "1" if the index is a partial index and "0" if not.
"""
index_list(db, table=""; kwargs...) = select(db; from="pragma_index_list('$table')", kwargs...)

"""
    index_info(db, index; kwargs...) --> pragma_index_info('index')

    selects from [pragam_index_info](https://www.sqlite.org/pragma.html#pragma_index_info),
    i.e. returns one row for each key column in the named index. A key column
    is a column that is actually named in the CREATE INDEX index statement or
    UNIQUE constraint or PRIMARY KEY constraint that created the index. Index
    entries also usually contain auxiliary columns that point back to the table
    row being indexed. The auxiliary index-columns are not shown by the
    index_info pragma, but they are listed by the index_xinfo pragma.

    1. The rank of the column within the index. (0 means left-most.)
    2. The rank of the column within the table being indexed. A value of -1 means rowid
       and a value of -2 means that an expression is being used.
    3. The name of the column being indexed. This columns is NULL if the column is the rowid or an expression.
"""
index_info(db, index=""; kwargs...) = select(db; from="pragma_index_info('$index')", kwargs...)

"""
    index_xinfo(db, index; kwargs...) --> pragma_index_xinfo('index')

    selects from [pragam_index_xinfo](https://www.sqlite.org/pragma.html#pragma_index_xinfo),
    i.e. returns information about every column in an index. Unlike index_info,
    information about every column in the index, not just the key columns is
    returned. Output columns from index_xinfo are

    1. The rank of the column within the index. (0 means left-most. Key columns come before auxiliary columns.)
    2. The rank of the column within the table being indexed, or -1 if the
       index-column is the rowid of the table being indexed and -2 if the index is on an expression.
    3. The name of the column being indexed, or NULL if the index-column is the rowid of the table being indexed
       or an expression.
    4. 1 if the index-column is sorted in reverse (DESC) order by the index and 0 otherwise.
    5. The name for the collating sequence used to compare values in the index-column.
    6. 1 if the index-column is a key column and 0 if the index-column is an auxiliary column.
"""
index_xinfo(db, index=""; kwargs...) = select(db; from="pragma_index_xinfo('$index')", kwargs...)

struct Column_metadata
    datatype::String
    collseq::String
    notnull::Bool
    primkey::Bool
    autoinc::Bool
end

function table_column_metadata(db::DB, tblname, colname; dbname=Ptr{UInt8}(C_NULL))
    datatype = Vector{Ptr{UInt8}}(undef, 1)
    collseq  = Vector{Ptr{UInt8}}(undef, 1)
    notnull  = Int32(0)
    primkey  = Int32(0)
    autoinc  = Int32(0)

    iret = ccall( (@s3 table_column_metadata), Cint,
                 (Ptr{Cvoid}, Ptr{UInt8}, Ptr{UInt8}, Ptr{UInt8},
                  Ptr{Ptr{UInt8}}, Ptr{Ptr{UInt8}}, Ref{Int32}, Ref{Int32}, Ref{Int32}),
                 db.handle, dbname, tblname, colname,
                 datatype, collseq, notnull, primkey, autoinc)
    iret!=SQLITE_OK && throw( SqliteError("Sqlite: not a table, but a view"))
    Column_metadata(unsafe_string(datatype[1]), unsafe_string(collseq[1]),
                    Bool(notnull), Bool(primkey), Bool(autoinc))
end

databases(db::DB) = collect(String, StmtLen(db; columns="name", from="pragma_database_list"))
databases(dbnm) = databases(opendb(dbnm))

"""
    attach(db, dbnm, as) -> return_code

adds another database (SQLite file) to the current database connection,
[see also] (https://www.sqlite.org/lang_attach.html). Alternatively 

    attach(db, ("my_sqlite.db", "a"))

would attach file "my_sqlite.db" as database "a".
"""
attach(db::DB, dbnm, as) = exec(db, "ATTACH \"$dbnm\" AS $as")
attach(db::DB, att) = exec(db, "ATTACH \"$(att[1])\" AS $(att[2])")
detach(db::DB, as) = exec(db, "DETACH $as")

foreign_keys(db::DB, on) = exec(db, "PRAGMA foreign_keys = "*(on ? "ON" : "OFF")*";")

wal_autocheckpoint(db::DB, n) =
ccall((@s3 wal_autocheckpoint), Cint, (Ptr{Cvoid}, Cint), db.handle, n)

"""
    wal_checkpoint(::DB; mode=SQLITE_CHECKPOINT_PASSIVE) -> (size of WAL log in frames, total number of frames checkpointed)

    ccalls [sqlite3_wal_checkpoint_v2](https://www.sqlite.org/c3ref/wal_checkpoint_v2.html).
    mode is one of
    - SQLITE_CHECKPOINT_PASSIVE  = 0  #/* Do as much as possible w/o blocking */
    - SQLITE_CHECKPOINT_FULL     = 1  #/* Wait for writers, then checkpoint */
    - SQLITE_CHECKPOINT_RESTART  = 2  #/* Like FULL but wait for for readers */
    - SQLITE_CHECKPOINT_TRUNCATE = 3  #/* Like RESTART but also truncate WAL */
"""
function wal_checkpoint(db::DB, attached="";
        mode=SQLITE_CHECKPOINT_PASSIVE)
    logszp  = Array{Cint}(undef, 1)
    chkptdp = Array{Cint}(undef, 1)
    # logszp  = Cint(0)
    # chkptdp = Cint(0)
    iret = ccall((@s3 wal_checkpoint_v2), Cint,
                 (Ptr{Cvoid}, Ptr{UInt8}, Cint, Ptr{Cint},Ptr{Cint}),
                 db.handle, isempty(attached) ? C_NULL : attached, mode, logszp,   chkptdp)
    iret!=SQLITE_OK && throw( SqliteError(errmsg(db), "setting wal checkpoint on $attached") )
    return logszp[1],chkptdp[1]
    # return logszp,chkptdp
end
wal_checkpoint(dbnm, attached=""; mode=SQLITE_CHECKPOINT_PASSIVE) =
wal_checkpoint(opendb(dbnm, "w"), attached; mode)

function transaction(db::DB; immediate=false, exclusive=false)
    exec(db, "BEGIN"*(exclusive ? " EXCLUSIVE" : (immediate ? " IMMEDIATE" : "")*";"))
end
commit(db::DB)                = exec(db, "COMMIT;")
savepoint(db::DB; name="def") = exec(db, "SAVEPOINT $name;")
release(db::DB;   name="def") = exec(db, "RELEASE $name;")
rollback(db::DB;  to="")      = exec(db, "ROLLBACK"*(!isempty(to) ? " TO" : "")*";")

get_autocommit(db::DB) = ccall((@s3 get_autocommit), Cint, (Ptr{Cvoid},), db.handle)

extended_result_codes(db::DB, on=true) =
ccall((@s3 extended_result_codes), Cint, (Ptr{Cvoid}, Cint), db.handle,
      on ? Cint(1) : Cint(0))

extended_errcode(db::DB) = ccall((@s3 extended_errcode), Cint, (Ptr{Cvoid},), db.handle)

# Support the carray extension:
function pointerstr(v)
    str = "$(pointer(v))"
    idx = findfirst("@", str)[1]
    str[idx+1:end]
end
carray(v::Vector{Int32}, n::Int)   = "carray("*pointerstr(v)*",$n)"
carray(v::Vector{Int64}, n::Int)   = "carray("*pointerstr(v)*",$n,'int64')"
carray(v::Vector{Float64}, n::Int) = "carray("*pointerstr(v)*",$n,'double')"
carray(v::String)                  = "carray("*pointerstr(v)*",$(endof(v)),'char*')"

changes(db::DB)       = ccall((@s3 changes), Cint, (Ptr{Cvoid},), db.handle)
total_changes(db::DB) = ccall((@s3 total_changes), Cint, (Ptr{Cvoid},), db.handle)

stricmp(a::String, b::String) =
Bool(ccall((@s3 stricmp), Cint, (Cstring,Cstring), a, b))
strnicmp(a::String, b::String, sz) =
Bool(ccall((@s3 strnicmp), Cint, (Cstring,Cstring,Cint), a, b, Cint(sz)))
strlike(globstr::String, zstr::String, cesc::Char='\0') =
Bool(ccall((@s3 strlike), Cint, (Cstring,Cstring,Cint), globstr, zstr, Cint(cesc)))
strglob(globstr::String, zstr::String) =
Bool(ccall((@s3 strglob), Cint, (Cstring,Cstring), globstr, zstr))

# Tracing and Profiling functions

printsql(::Ptr{Any}, sqltxt::Ptr{UInt8}) = println(unsafe_string(sqltxt))::Void

function sql2io(io_::Ptr{IOStream}, sqltxt::Ptr{UInt8})
    io = unsafe_pointer_to_objref(io_)::IOStream
    println(io, unsafe_string(sqltxt))::Void
end

const printsql_c = @cfunction(printsql, Nothing, (Ptr{Any}, Ptr{UInt8}))
const sql2io_c = @cfunction(sql2io, Nothing, (Ptr{IOStream}, Ptr{UInt8}))

printsqlprof(::Ptr{Any}, sqltxt::Ptr{UInt8}, nanonsec::UInt64) =
println("$(unsafe_string(sqltxt)) -- $(nanosec/1e6) msec")::Void
function sqlprof2io(io_::Ptr{IOStream}, sqltxt::Ptr{UInt8}, nanonsec::UInt64)
    io = unsafe_load(io_)::IOStream
    println(io, "$(unsafe_string(sqltxt)) -- $(nanosec/1e6) msec")::Void
end

const printsqlprof_c = @cfunction(printsqlprof, Nothing, (Ptr{Any}, Ptr{UInt8}, Culong))
const sqlprof2io_c   = @cfunction(sqlprof2io, Nothing, (Ptr{IOStream}, Ptr{UInt8}, Culong))

profile(db::DB) =
    ccall((@s3 profile), Ptr{Cvoid}, (Ptr{Cvoid}, Ptr{Cvoid}, Ptr{Cvoid}), db.handle, printsqlprof_c, C_NULL)
profile(db::DB, io::IOStream) =
    ccall((@s3 profile), Ptr{Cvoid}, (Ptr{Cvoid}, Ptr{Cvoid}, Ptr{Cvoid}),
          db.handle, sqlprof2io_c, pointer_from_objref(io))

"""
    AbstractStmt

is parent to objects having a handle to SQLite prepared statement.
"""
abstract type AbstractStmt end
"""
    handle(stmt) -> the handle to SQLite statements or databases.
"""
handle(stmt)    = stmt.handle

ncols(stmt::AbstractStmt)     = ccall((@s3 column_count), Cint, (Ptr{Cvoid},), handle(stmt))
eachindex(stmt::AbstractStmt) = 0:ncols(stmt)-1
nbindpars(stmt)               = ccall((@s3 bind_parameter_count), Cint, (Ptr{Cvoid},), handle(stmt))

"""
    SqlStmt(handle) -> SQLite statement which needs to be finalized explicitely
"""
struct SqlStmt <: AbstractStmt
    handle::Ptr{Cvoid}
    ncols::Cint
    nbindpars::Cint
    function SqlStmt(h)
        nc = ccall((@s3 column_count),         Cint, (Ptr{Cvoid},), h)
        nb = ccall((@s3 bind_parameter_count), Cint, (Ptr{Cvoid},), h)
        return new(h, nc, nb)
    end
end

"""
    AutoStmt(handle) -> SQLite statement which is finalized automatically some time after going out of scope
"""
mutable struct AutoStmt <: AbstractStmt
    handle::Ptr{Cvoid}
    ncols::Cint
    nbindpars::Cint
    function AutoStmt(h; finizer=true)
        nc = ccall((@s3 column_count),         Cint, (Ptr{Cvoid},), h)
        nb = ccall((@s3 bind_parameter_count), Cint, (Ptr{Cvoid},), h)
        ns = new(h, nc, nb)
        finizer && finalizer(finalize!, ns)
        return ns
    end
end
"""
    ncols(stmt) -> number of columns of a prepared statement or the columns string of textual SQL statement
"""
ncols(stmt::Union{SqlStmt, AutoStmt})     = stmt.ncols
nbindpars(stmt::Union{SqlStmt, AutoStmt}) = stmt.nbindpars
"""
    null!(stmt) -> mutes the handle of stmt to NULL
"""
function null!(stmt::Union{SqlStmt,AutoStmt})
    stmt.handle=C_NULL
end

function show(io::IO, stmt::AbstractStmt)
    if handle(stmt) == C_NULL
        print(io, "Statement not prepared")
    else
        # print(io, "SQL: $(sql(stmt))\nncols: $(stmt.ncols), nbindpars: $(stmt.nbindpars)")
        print(io, "SQL: $(sql(stmt))")
    end
end
db_status(stmt::AbstractStmt, op; reset=false) =
ccall((@s3 db_status), Cint, (Ptr{Cvoid}, Cint, Cint), handle(stmt), op, Cint(reset))

throw_stmt(iret::Cint) = throw( SqliteError(errstr(iret)) )
throw_stmt(stmt::AbstractStmt) =
throw( SqliteError(errmsg(db_handle(stmt)), sql(stmt) ))

struct StmtError <: Exception
    stmt::AbstractStmt
    code::Cint
end
function show(io::IO, err::StmtError)
    if handle(err.stmt)==C_NULL
        print(io, "finalized statement?\n")
    else
        print(io, "$(sql(err.stmt)) :\n")
        print(io, "Sqlite error code $(err.code): $(errstr(err.code))\n")
        print(io, errmsg(db(err.stmt)))
    end
end

column_database_name(stmt::AbstractStmt, kc) =
    unsafe_string( (@s3 column_database_name), Ptr{UInt8}, (Cint,), kc)
column_table_name(stmt::AbstractStmt, kc) =
    unsafe_string( (@s3 column_table_name), Ptr{UInt8}, (Cint,), kc)
column_origin_name(stmt::AbstractStmt, kc) =
    unsafe_string( (@s3 column_origin_name), Ptr{UInt8}, (Cint,), kc)

next_stmt(handle::Ptr{Cvoid}, stmtp=C_NULL) =
    ccall((@s3 next_stmt), Ptr{Cvoid}, (Ptr{Cvoid}, Ptr{Cvoid}), handle, stmtp)
next_stmt(db::DB, stmtp=C_NULL) = next_stmt(handle(db), stmtp)

sql(handle) = unsafe_string(ccall((@s3 sql), Ptr{UInt8}, (Ptr{Cvoid},), handle))
sql(stmt::AbstractStmt) = sql(handle(stmt))
sql(stmt::AutoStmt) = handle(stmt)!=C_NULL ? sql(handle(stmt)) : ""
function expanded_sql(handle)
    ptr = ccall((@s3 expanded_sql), Ptr{UInt8}, (Ptr{Cvoid},), handle)
    expsql = unsafe_string(ptr)
    free(ptr)
    return expsql
end
expanded_sql(stmt::AbstractStmt) = expanded_sql(handle(stmt))
expanded_sql(stmt::AutoStmt) = handle(stmt)!=C_NULL ? expanded_sql(handle(stmt)) : nothing

const conflicts = ("ABORT", "FAIL", "IGNORE", "REPLACE", "ROLLBACK")

"""
    complete(::AbstractString) calls [sqlite3_complete](https://www.sqlite.org/c3ref/complete.html)
"""
complete(sql::AbstractString) = Bool( ccall((@s3 complete), Cint, (Ptr{UInt8},), sql) )

"""
    prepare(::DB, ::String[, SqlStmt|Statement|StmtLen]) -> statement for SQLite

    The default return 
"""
function prepare(db, sql::String, ::Type{S}=AutoStmt) where {S<:AbstractStmt}
    stmthp = Ref(C_NULL)
    if ccall((@s3 prepare_v2), Cint,
             (Ptr{Cvoid}, Ptr{UInt8}, Cint, Ptr{Cvoid}, Ptr{UInt8}),
             db.handle,  sql, sizeof(sql), stmthp, C_NULL)!=SQLITE_OK
        # iret = ccall((@s3 prepare_v2), Cint,
        #               (Ptr{Cvoid}, Ptr{UInt8}, Cint, Ptr{Cvoid}, Ptr{UInt8}),
        #               db.handle,  sql, sizeof(sql), stmthp, C_NULL)
        # if iret!=SQLITE_OK
        #     @show iret
        #     @show errmsg(db)
        #     @show errstr(iret)
        throw( SqliteError(errmsg(db), sql))
    end
    return S(stmthp[])
end # function prepare_v2

function prepare_m(db, sql::String, ::Type{S}=AutoStmt) where {S<:AbstractStmt}
    stmtp = Ref(C_NULL)
    tailp = Ref{Ptr{UInt8}}()
    iret = ccall((@s3 prepare_v2), Cint,
                 (Ptr{Cvoid}, Ptr{UInt8}, Cint, Ptr{Cvoid}, Ptr{Ptr{UInt8}}),
                 db.handle,  sql, sizeof(sql), stmtp, tailp)
    iret!=SQLITE_OK && throw( SqliteError(errmsg(db), sql))
    return S(stmtp[]),unsafe_string(tailp[])
end # function prepare_v2

"prepend(table::String, schema::String) -> \"schema.table\"

utility to prepend schema names to table names"
prepend(table::String, schema::String) = isempty(schema) ? table : "$(schema).$table"

"addclause(sql, clause, word:) -> prepended SQL string

appends a partial SQL statement with a clause. The clause name/word (like
FROM, WHERE...) is prepended if the clause string does not already start with
the name. Uppercase `word` is assumed."
function addclause(sql, clause, word)
    r = Regex("^\\s*$(word)\\s{1,}")
    if occursin(r, clause)
        return "$sql $(replace(clause, r => word))"
    elseif !isempty(clause)
        return "$sql $word $clause"
    else
        return sql
    end
end

addrecurs(sqltxt, recursive) =
    occursin(r"^\s*(?i)WITH", sqltxt) && recursive && !occursin(r"^\s*(?i)WITH\s{1,}RECURSIVE ", sqltxt) ?
    replace(sqltxt, r"^\s*(?i)WITH" => s"WITH RECURSIVE") : sqltxt

withclause(with, recursive) =
    addclause("",  with, "WITH") |> lstrip |> x -> addrecurs(x, recursive) |> x -> !isempty(x) ? x*' ' : x

app_semcol(sql) = complete(sql) ? sql : "$sql;"

function prepare_select(db::DB, ::Type{S}=AutoStmt; sql="", dostep=false, kwargs...) where {S<:AbstractStmt}
    if isempty(sql); sql = sql_select(; kwargs...); end
    stmt = prepare.(Ref(db), sql, Ref(S))
    dostep && step.(stmt)

    stmt
end

"""
    prepare_select(...) do stmt
        ...
    end

    enables `do` block syntax, for encapsulating in local scope and automatically finalizing the statement. 
"""
function prepare_select(func::Function, args...; kwargs...)
    stmt = prepare_select(args...; kwargs...)
    func(stmt)
end

# Database can be calles like prepare_select:
(db::DB)(args...; kwargs...) = prepare_select(db, args...; kwargs...)
function (db::DB)(func::Function, args...; kwargs...)
    stmt = prepare_select(db, args...; kwargs...)
    func(stmt)
end

function sql_select(; with="", columns="*", values="", from="", where="",
        groupby="", window="", order="", limit="", offset="", recursive=false,
        distinct=false, withcount=false, onlycount=false)
    @assert(isempty(values) || ((isempty(columns) || cmp(columns, "*")==0) && isempty(from)
                                && !withcount && !onlycount),
            "If VALUES, columns, from, withcount and onlycount cannot be specified")
    @assert(!onlycount || isempty(columns) || cmp(columns, "*")==0,
            "If only counting then columns cannot be specified")

    if !onlycount
        sqltxt = withclause(with, recursive)
    end
    if withcount || onlycount
        cnttxt = withclause(with, recursive)*"SELECT count(*)"
    end

    # SELECT core can start with "SELECT" or "VALUES"
    if isempty(values)
        if !onlycount # result columns
            selstr = "SELECT"
            if columns isa AbstractString # columns is allowed to begin with "SELECT ..."
                if occursin(r"^\s*SELECT\s{1,}", columns)
                    @assert(!withcount,
                            "For counting specify just columns, not \"SELECT ... \" ")
                    selcol = columns
                else
                    selstr = "SELECT"
                    if distinct
                        selstr = "$selstr DISTINCT"
                    end
                    selcol = "$selstr $(prepare_selcols(columns))"
                end
            else
                selcol = "$selstr $(prepare_selcols(columns))"
            end
            sqltxt = length(sqltxt)>0 ? "$sqltxt$selcol" : selcol
        end

        afrmtxt = addclause("", from, "FROM")
        afrmtxt = addclause(afrmtxt, where, "WHERE")
        afrmtxt = addclause(afrmtxt, groupby, "GROUP BY")
        afrmtxt = addclause(afrmtxt, window, "WINDOW")
        if !onlycount
            sqltxt  = join((sqltxt, afrmtxt))
        end
        if withcount || onlycount
            cnttxt  = join((cnttxt, afrmtxt))
        end
    else
        if occursin(r"^\s*VALUES\s{1,}", values)
            valtxt = values
        else
            valtxt = "VALUES $values"
        end
        sqltxt = length(sqltxt)>0 ? "$sqltxt $valtxt" : valtxt
    end

    afrmtxt = addclause("", order, "ORDER BY")
    afrmtxt = addclause(afrmtxt, string(limit), "LIMIT")
    afrmtxt = addclause(afrmtxt, string(offset), "OFFSET")

    if !onlycount
        sqltxt  = join((sqltxt, afrmtxt))
    end
    if onlycount || withcount
        cnttxt = join((cnttxt, afrmtxt))
    end
    if onlycount
        return cnttxt
    else
        return withcount ? (sqltxt, cnttxt) : sqltxt
    end
end
nt(db... ; save=false, kwargs...) = kwargs

"""
    StmtCount(::DB, bindpar=(); sel_kwargs...)

constructs a statement that knows the nr of selected rows; properties `stmt` and `cnt`.
Warning: `cnt` can become invalid, if rows are inserted or deleted, or statement parameters are rebound.
"""
mutable struct StmtLen <: AbstractStmt
    stmt::AutoStmt
    cntstmt::AutoStmt
    cnt::Int
end
handle(sl::StmtLen)  = handle(sl.stmt)
function StmtLen(db::DB, bindpar=(); kwargs...)
    stmt,cntstmt = prepare_select(db, AutoStmt; withcount=true, kwargs...)
    if !isempty(bindpar)
        bind(stmt, bindpar)
        bind(cntstmt, bindpar)
    end
    step(cntstmt)
    StmtLen(stmt, cntstmt, Int64(cntstmt))
end
iterate(sl::StmtLen, state=SQLITE_OK) = iterate(sl.stmt, state)
length(sl::StmtLen) = sl.cnt
IteratorSize(::Type{StmtLen}) = HasLength()

function bind(sl::StmtLen, vals)
    bind(sl.stmt, vals)
    reset(sl.cntstmt)
    bind(sl.cntstmt, vals)
    step(sl.cntstmt)
    sl.cnt = Int64(sl.cntstmt)
end

"""
    |>(x, insstmt::AbstractStmt)

Inserts a row `x` into the database using a prepared statement `insstmt`, calling checked
`bind` and `step` functions. This enables a pipe syntax:

# Example
```jldoctest
julia> using Random

julia> db = opendb(); exec(db, "CREATE TABLE locations(id INTEGER PRIMARY KEY, name TEXT, latitude REAL, longitude REAL);")
0

julia> insstmt = prepare_insert(db, into="locations")
SQL: INSERT INTO locations VALUES (?1,?2,?3,?4);

julia> gen = ((nothing, randstring(vcat('A':'Z', 'a':'z')), (rand() .- .5).*180, (rand() .- .5).*360 for k in 1:128)
Base.Generator{UnitRange{Int64}, var"#7#8"}(var"#7#8"(), 1:999)

julia> gen .|> insstmt # Broadcasting is supported

```
"""
function |>(x, insstmt::AbstractStmt)
    checked_bind(insstmt, x)
    checked_step(insstmt)
    reset(insstmt)
end
function Broadcast.broadcasted(::typeof(|>), arg, insstmt::AbstractStmt)
    for x in arg
        x |> insstmt
    end
end

|>(x::Union{Vector, Tuple}, insstmt::AbstractStmt) = insert(insstmt, x)

"""
    prepare_inscols(cols) --> string indicating the columns to insert 

    i.e. puts brackets around if needed
"""
prepare_inscols(cols::AbstractString) = cols[1]!="(" ? "($cols)" : cols
prepare_inscols(cols) = "($(rstrip(mapfoldl(x -> x*',', *, cols), ',')))"
"""
    prepare_selcols(cols) --> string indicating the columns to select

    i.e. removes brackets around if needed
"""
prepare_selcols(coltxt::AbstractString) = coltxt
prepare_selcols(cols) = "$(rstrip(mapfoldl(x -> x*',', *, cols), ','))"

column_count(coltxt::AbstractString) = length(findall(',', coltxt))+1 
column_count(cols) = length(cols)

function prepare_updcols(cols::AbstractString)
    coltxt = ""
    kommapos = findall(',', cols)
    from = 1
    for (k,km) in enumerate(kommapos)
        coltxt *= "$(cols[from:km-1])=?$k,"
        from = km+1
    end
    "$(coltxt)$(cols[from:end])=?$(length(kommapos)+1)"
end
prepare_updcols(cols) =
rstrip(mapfoldl(x -> "$(x[2])=?$(x[1]),", *, enumerate(cols)), ',')

function prepare_insert(db::DB, ::Type{S}=AutoStmt; sql="", kwargs...) where {S<:AbstractStmt}
    if isempty(sql)
        sql = sql_insert(db; kwargs...)
    end
    prepare(db, complete(sql) ? sql : "$sql;", S)
end

"""
    prepare_insert(...) do stmt
        ...
    end

    works with the usual `do` block having local scope and automatic finalizing the statement. 
"""
function prepare_insert(func::Function, args...; kwargs...)
    stmt = prepare_insert(args...; kwargs...)
    func(stmt)
end

function sql_insert(db; with="", into="", columns="", values="", select="", default=false,
        returning="", recursive=false, conflict=missing, replace=false)
    if !ismissing(conflict)
        @assert(uppercase(string(conflict)) ∈ conflicts, "conflict must be one of $conflicts")
        @assert(!replace, "if `replace` is true, no other conflicts can be used")
    end

    if replace
        @assert(!occursin(r"^\s*(?i)INSERT\s{1,}", into),
                "if `replace` is true, the `into` clause cannot start with \"INSERT\"")
    end

    # Ensure that one of the basic forms of INSERT (https://www.sqlite.org/lang_insert.html) 
    # is unambiguously specified. 
    if !isempty(values)
        @assert(isempty(select),
                "Ambiguous INSERT: VALUES is assigned and also inserting from a SELECT statement?")
        @assert(!default,
                "Ambiguous INSERT: VALUES is assigned and also inserting DEFAULT is true?")
    end
    if !isempty(select)
        @assert(!default,
                "Ambiguous INSERT: both from a SELECT statement and also DEFAULT is true?")
    end

    sqltxt = withclause(with, recursive)

    # @show into
    # if occursin(r"^\s*(?i)INSERT\s{1,}INTO", into) && !ismissing(conflict)
    if !ismissing(conflict)
        if occursin(r"^\s*(?i)INSERT\s{1,}INTO", into)
            insor = SubstitutionString("INSERT OR $(uppercase(string(conflict)))")
            into = replace(into, r"^\s*(?i)INSERT" => insor)
        else
            into = "INSERT OR $(uppercase(string(conflict))) INTO $into"
        end
    elseif !occursin(r"^\s*(?i)REPLACE\s{1,}INTO", into)
        if replace
            into = "REPLACE INTO $into"
        elseif !occursin(r"^\s*(?i)INSERT\s{1,}OR", into) 
            insstr = "INSERT"
            if !ismissing(conflict)
                insstr *= " OR $(uppercase(string(conflict)))"
            end
            into = "$insstr INTO $into"
        end
    end
    sqltxt = lstrip(sqltxt)*into

    if length(columns)>0 
        sqltxt = rstrip(sqltxt)*prepare_inscols(columns)
    end

    if isempty(select) && !default  
        if isempty(values)
            if isempty(columns)
                tblnm = strip(match(r"(?<=\bINTO\b\s)(?s)(.*$)", sqltxt).match)
                nval  = column_count(db, tblnm)
            else
                nval = column_count(columns)
            end
            sqltxt *= " VALUES("
            for k=1:nval
                sqltxt *= "?$(k),"
            end
            if nval==0
                @warn "table or view $into has zero columns or does not exist"
            else
                sqltxt = rstrip(sqltxt,  ',')
            end
            sqltxt *= ")"
        else
            # if length(values)<6 || getindex(uppercase(values), 1:6)!="VALUES"
            if values isa AbstractString 
                if !occursin(r"^\s*VALUES\s{1,}", values)
                    sqltxt = "$(rstrip(sqltxt)) VALUES($values)"
                else
                    sqltxt = "$sqltxt $values"
                end
            else
                sqltxt = "$(rstrip(sqltxt)) VALUES"
                foreach(values) do x
                    sqltxt *= "($x),"
                end
                sqltxt = chop(sqltxt)
            end
        end
    elseif default
        sqltxt *= " DEFAULT VALUES"
    else
        sqltxt *= " "*select
    end
    addclause(sqltxt, returning, "RETURNING")
end

explain_stmt!(stmt, mode=EXPLAIN_QUERY_PLAN) =
ccall((@s3 stmt_explain), Cint, (Ptr{Cvoid},Cint), handle(stmt), mode)
explain(stmt) = ccall((@s3 stmt_isexplain), Cint, (Ptr{Cvoid},), handle(stmt))

"
MatrixColumns is a type for iterating over the columns of a matrix as vectors
"
struct MatrixColumns{T}; m::Matrix{T}; end
length(a::MatrixColumns) = size(a.m, 2)
iterate(a::MatrixColumns, state=1) = state>length(a) ? nothing : (vec(a.m[:, state]), state+1)
eltype(::Type{MatrixColumns{T}}) where T = Vector{T}
"
columns(m) is a function for iterating over the columns of a matrix `m` as vectors
"
columns(m::Matrix{T}) where T = MatrixColumns(m)

function insert(stmt::AbstractStmt, v::Union{Vector, Tuple})
    if isempty(v)
        try
            checked_step(stmt)
        finally
            checked_reset(stmt)
        end
        return
    end
    if nfields(v[1])==0    # vector/tuple of vectors/scalars
        lengths = ( isa(_v, Union{String, Blob, Base.ReinterpretArray{UInt8}, Nothing, Missing}) ?
                   1 : length(_v) for _v in v )
        maxln   = maximum(lengths)
        if maxln==1    # vector/tuple of scalars/strings/blobs
            bind(stmt, v)
            try
                checked_step(stmt)
            finally
                checked_reset(stmt)
            end
        else           # vector/tuple of vectors
            for _k in 1:maxln
                @debug "row = " [isa(_col, Union{String, Blob}) ? _col : _col[minimum([_l, _k])] for (_col,_l) in zip(v, lengths)] 
                iret = bind(stmt, [isa(_col, Union{String, Blob}) ? 
                                   _col : 
                                   _col[minimum([_l, _k])] for (_col,_l) in zip(v, lengths)])
                try
                    checked_step(stmt)
                finally
                    checked_reset(stmt)
                end
            end
        end
    else    # vector/tuple of structs, must not be nested
        for _v in v
            # vector/tuple of scalars/strings/blobsbind(stmt, _v)
            iret = bind(stmt, _v)
            try
                checked_step(stmt)
            finally
                checked_reset(stmt)
            end
        end
    end
end

function insert(stmt::AbstractStmt, m::Matrix)
    for k in 1:size(m, 2)
        bind(stmt, m[:, k])
        try
            checked_step(stmt)
        finally
            reset(stmt)
        end
    end
end

function insert(stmt::AbstractStmt, notnested)
    bind(stmt, notnested)
    try
        checked_step(stmt)
    finally
        reset(stmt)
    end
end

insert(db::DB; idata=[], inspars...) = insert(prepare_insert(db; inspars...), idata)
insert(db::DB, idata; inspars...) = insert(prepare_insert(db; inspars...), idata)
insert(dbnm; kwargs...) = insert(opendb(dbnm, "w"); kwargs...)

#    insert(prepare_insert(db; havedata=!isempty(idata), inspars...), idata)

"""
    prepare_update(::DB; sql, with, table, set, columns, where) --> prepared UPDATE statement

    see the [UPDATE syntax diagram](https://www.sqlite.org/lang_update.html)

Keywords:

  * sql       complete, self-contained SQL, String, if given, then all other keywords are ignored;
  * with      WITH clause, String, RECURSVIE is added if `recursive==true`;
  * table     [qualified table name](https://www.sqlite.org/syntax/qualified-table-name.html), String;
  * set       self-contained SET clause, including an optional FROM clause,
              String, `SET` is prepended if `!startswith(set, "SET ")`
  * columns   columns for the SET clause, String  or collection of Strings,
              Examples: "a,bb" or ("a", "bb") become "SET a=?1,bb=?2"
  * where     WHERE clause, which rows to update
  * returning RETURNING clause, String, `RETURNING` is prepended;
  * conflict  adds a conflict clause ` OR ` conflict. 
"""
prepare_update(db::DB; sql="", kwargs...) =
    isempty(sql) ? prepare(db, sql_update(; kwargs...)) : prepare(db, sql)

"""
    prepare_update(...) do stmt
        ...
    end

    works with the usual `do` block having local scope and automatic finalizing the statement. 
"""
function prepare_update(func::Function, args...; kwargs...)
    stmt = prepare_update(args...; kwargs...)
    func(stmt)
end

function sql_update(; with="", table="", set="", columns="", where="",
        returning="", recursive=false, conflict=missing)
    if !ismissing(conflict)
        @assert(uppercase(string(conflict)) ∈ conflicts, "conflict must be one of $conflicts")
    end

    if isempty(set)
        @assert(length(columns)>=1, "At least one column needs to be updated")
    else
        @assert(isempty(columns), "Either a SET clause or columns can be specified")
    end

    sqltxt = withclause(with, recursive)

    # if startswith(uppercase(table), "UPDATE ")
    if occursin(r"^\s*UPDATE\s{1,}", table)
        if !ismissing(conflict)
            updor = SubstitutionString("UPDATE OR $(uppercase(string(conflict)))")
            table = replace(table, r"^\s*(?i)UPDATE" => updor)
        end
    else
        sqltxt = lstrip("$sqltxt UPDATE")
        if !ismissing(conflict)
            sqltxt *= " OR $(uppercase(string(conflict)))"
        end
    end
    sqltxt = lstrip("$sqltxt $table ")

    if isempty(set)
        sqltxt *= "SET "*prepare_updcols(columns)
    else
        sqltxt = addclause(String(sqltxt), set, "SET")
    end

    sqltxt = addclause(sqltxt, where, "WHERE")
    sqltxt = addclause(sqltxt, returning, "RETURNING")

    if !endswith(sqltxt, ';')
        sqltxt *= ';'
    end
end

update(stmt::AbstractStmt, bindpar) = insert(stmt, bindpar)
update(db::DB; values=[], wherepar=[], updpars...) =
    update(prepare_update(db; updpars...), isempty(wherepar) ? values : [values; wherepar])

function prepare_delete(db::DB, ::Type{S}=AutoStmt;
        with::String="", from="", where::String="") where {S<:AbstractStmt}
    sqltxt = addclause("",  with, "WITH")

    if length(from)<6 || getindex(uppercase(from), 1:6)!="DELETE"
        sqltxt = length(sqltxt)>0 ? "$sqltxt DELETE FROM $from" : "DELETE FROM $from"
    else
        sqltxt = length(sqltxt)>0 ? "$sqltxt $from" : from
        sqltxt = addclause(sqltxt, where,  "WHERE")
        sqltxt = sqltxt[end]!=';' ? "$(sqltxt);" : sqltxt
    end
    prepare(db, sqltxt, S)
end

function delete(stmt::AbstractStmt)
    try checked_step(stmt)
    finally reset(stmt)
    end
end
function delete(stmt::AbstractStmt, bindpar)
    bind(stmt, bindpar)
    delete(stmt)
end

function delete(db::DB; bindpar=(), delkws...)
    stmt = prepare_delete(db; delkws...)
    isempty(bindpar) ? delete(stmt) : delete(stmt, bindpar)
end

finalize_stmt(h::Ptr{Cvoid}) = ccall((@s3 finalize), Cint, (Ptr{Cvoid},), h)

# function checked_finalize(stmt::Union{SqlStmt,AutoStmt})
function checked_finalize(stmt)
    # @async @show handle(stmt),sql(stmt)
    dbh  = db_handle(stmt)
    dbh==C_NULL && throw( StmtError("statement has no db handle") )
    stptr = next_stmt(dbh)
    # @async @show dbh,stptr
    # stptr==C_NULL && throw( StmtError("no active statements?") )
    while stptr!=C_NULL
        if handle(stmt)==stptr
            iret = finalize_stmt(handle(stmt))
            if iret != SQLITE_OK
                println("when trying to finalize $(sql(stmt)):\n iret = $iret, errmsg = $(errmsg(dbh))")
                reset(stmt)
                finalize_stmt(handle(stmt)) == SQLITE_OK && println("ok after resetting")
            end
            # iret != SQLITE_OK &&  throw( SqliteError("error when finalizing", sql(stmt)) )
            # finalize_stmt(handle(stmt))!=SQLITE_OK &&  throw( SqliteError("error when finalizing", sql(stmt)) )
            break
        end
        stptr = next_stmt(dbh, stptr)
    end
    # stptr==C_NULL && throw( StmtError("not in list of active statements") )
    @debug stptr==C_NULL && @async println("Statement was not finalized")
end

function finalize!(stmt::Union{SqlStmt,AutoStmt})
    if isnothing(stmt.handle) || stmt.handle==C_NULL
        StmtError("cannot finalize statement with handle \"nothing\"")
    end
    checked_finalize(stmt)
    null!(stmt)
end

function list_unfinalized(db::DB)
    stptr = next_stmt(db)
    # if stptr==C_NULL
    #     println("no unfinalized statements")
    # end
    while stptr!=C_NULL
        println("$(stptr): $(sql(stptr))")
        stptr = next_stmt(db, stptr)
    end
end

function finalize_all(db::DB)
    stptr = next_stmt(db)
    cnt = 0
    while stptr!=C_NULL
        finalize_stmt(stptr)
        cnt += 1
        stptr = next_stmt(db, stptr)
    end
    @debug @async println("$cnt statements were finalized")
end # function finalize_all

function finalize_close!(db::AutocloseDB)
    # if next_stmt(db)!=C_NULL
    #     println("unfinalized statements!")
    #     list_unfinalized(db)
    # end
    finalize_all(db)
    close!(db)
end

function finalize_close(db::SqliteDB)
    finalize_all(db)
    close(db)
end # function finalize_close

db_handle(stmt::AbstractStmt) = ccall((@s3 db_handle), Ptr{Cvoid}, (Ptr{Cvoid},), handle(stmt))

db(stmt::AbstractStmt) = SqliteDB(db_handle(stmt))

readonly(stmt::AbstractStmt) = Bool(ccall((@s3 stmt_readonly), Cint, (Ptr{Cvoid},), handle(stmt)))

busy(stmt::AbstractStmt) = Bool(ccall((@s3 stmt_busy), Cint, (Ptr{Cvoid},), handle(stmt)))

# using @threadcall turned out to be ~20 times slower
# step(stmt::AbstractStmt) = @threadcall((@s3 step), Cint, (Ptr{Cvoid},), handle(stmt))
step(stmt::AbstractStmt) = ccall((@s3 step), Cint, (Ptr{Cvoid},), handle(stmt))::Int32
reset(stmt::AbstractStmt) = ccall((@s3 reset), Cint, (Ptr{Cvoid},), handle(stmt))::Int32

function check(iret::Cint, stmt::AbstractStmt)
    (iret==SQLITE_ROW || iret==SQLITE_DONE || iret==SQLITE_OK) && return iret
    iret==SQLITE_BUSY && throw( SqliteBusy() )
    throw( StmtError(stmt, iret) )
end

checked_step(stmt) = check(step(stmt), stmt)
checked_reset(stmt) = check(reset(stmt), stmt)

clear_bindings(stmt) = ccall((@s3 clear_bindings), Cint, (Ptr{Cvoid},), handle(stmt))

bind_parameter_count(stmt) = ccall((@s3 bind_parameter_count), Cint, (Ptr{Cvoid},), handle(stmt))

checkbindbounds(stmt, k) = (k<1 || k>nbindpars(stmt)) && throw(BoundsError(stmt, k))

bind_parameter_name(stmt, k) =
    unsafe_string(ccall((@s3 bind_parameter_name), Ptr{UInt8}, (Ptr{Cvoid},Cint), handle(stmt), k))

bind_parameter_index(stmt, nm) =
    ccall((@s3 bind_parameter_index), Cint, (Ptr{Cvoid},Ptr{UInt8}), handle(stmt), nm)

const Blob = Union{Array{UInt8}, Base.ReinterpretArray{UInt8}}

"""
    bind(::AbstractStmt, indx, val) -> rc

    binds `val` to the parameter with 0-based nr `indx` of a prepared statement.

    Binding values to statements is done to insert data, to control `WHERE` and other clauses etc.

    Types with a corresponding one in SQLite are (of course) bound directly. These include
    `Int8`,`Int16`,`Int32`,`UInt8`,`UInt16`, `String`. The default is
    to bind a `serialized` presentation of `val`. See below for special cases.

   `indx` is bounds checked, omit with `@inbounds`. The returned `rc` should be `SQLITE_OK`.
"""
function bind end

"""
   bind(stmt, indx, ::Float64) -> rc

   binds a double/64-bit float value.

   See also

   [`sqlite3_bind_double`](https://www.sqlite.org/c3ref/bind_blob.html), `DOUBLE_IS_BLOB` and `NAN_IS_BLOB`

"""
@inline function bind(stmt, indx::Integer, val::Float64)
    @boundscheck checkbindbounds(stmt, indx)
    if DOUBLE_IS_BLOB[] || (NAN_IS_BLOB[] && isnan(val))
        ccall((@s3 bind_blob), Cint,
              (Ptr{Cvoid},   Cint, Ref{Float64}, Clong, Ptr{Cvoid}),
              handle(stmt), indx, val,          8,     destr_transient)
        # handle(stmt), indx, val,          8,     Ptr{Nothing}(SQLITE_TRANSIENT))
    else
        ccall((@s3 bind_double), Cint, (Ptr{Cvoid},Cint,Float64), handle(stmt), indx, val)
    end
end
# 
# @inline function bind(stmt, indx, val::Irrational)
#     @boundscheck checkbindbounds(stmt, indx)
#     ccall((@s3 bind_double), Cint, (Ptr{Cvoid},Cint,Float64), handle(stmt), indx, Float64(val))
# end

"""
   bind(stmt, indx, ::Union{Int8,Int16,Int32,UInt8,UInt16}) -> rc

   binds an integer value using the (usually 32 bits) C function  
   [`sqlite3_bind_int`](https://www.sqlite.org/c3ref/bind_blob.html)

"""
@inline function bind(stmt, indx::Integer, val::Union{Int8,Int16,Int32,UInt8,UInt16})
    @boundscheck checkbindbounds(stmt, indx)
    ccall((@s3 bind_int), Cint, (Ptr{Cvoid},Cint,Int32), handle(stmt), indx, val)
end

"""
   bind(stmt, indx, ::Union{Int64,UInt32}) -> rc

   binds an integer value using the 64 bits C function  
   [`sqlite3_bind_int64`](https://www.sqlite.org/c3ref/bind_blob.html)

"""
@inline function bind(stmt, indx::Integer, val::Union{Int64,UInt32})
    @boundscheck checkbindbounds(stmt, indx)
    ccall((@s3 bind_int64), Cint, (Ptr{Cvoid},Cint,Int64), handle(stmt), indx, val)
end
"""
    bind(::AbstractStmt, indx, ::UInt64) -> rc

    `UInt64` are bound as corresponding `Int64` 8-byte value, and can be retrieved with `column_uint64`.
    Within SQL values >2^63-1 appear negative.
"""
@inline function bind(stmt, indx::Integer, val::UInt64)
    @boundscheck checkbindbounds(stmt, indx)
    ccall((@s3 bind_int64), Cint, (Ptr{Cvoid},Cint,UInt64), handle(stmt), indx, val)
end

bind(stmt, indx, ch::Char) = bind(stmt, indx, string(ch))

"    bind(stmt, indx, ::Union{Missing, Nothing}) -> rc, missing and nothing are bound to NULL"
@inline function bind(stmt, indx::Integer, ::Union{Missing, Nothing})
    @boundscheck checkbindbounds(stmt, indx)
    ccall((@s3 bind_null), Cint, (Ptr{Cvoid},Cint), handle(stmt), indx)
end

"""
    bind(::AbstractStmt, indx, ::String, destr=destr_transient) -> rc

    Strings are bound as SQLite text. The `destr` argument indicates 
    whether a copy of the string needs to be made, the default is yes.

    See also `Litesql.destr_transient` and `Litesql.destr_static`
"""
@inline function bind(stmt, indx::Integer, str::String, destr=destr_transient)
    @boundscheck checkbindbounds(stmt, indx)
    ccall((@s3 bind_text), Cint,
          (Ptr{Cvoid},   Cint, Ptr{UInt8}, Cint,            Ptr{Cvoid}),
          handle(stmt), indx, str,        ncodeunits(str), destr)
end

@inline function bind(stmt, indx::Integer, blob::Blob, destr=destr_transient)
    @boundscheck checkbindbounds(stmt, indx)
    ccall((@s3 bind_blob64), Cint,
          (Ptr{Cvoid},   Cint, Ptr{UInt8}, Int64,        Ptr{Cvoid}),
          handle(stmt), indx, blob,       length(blob), destr)
end

"""
    bind(::AbstractStmt, indx, ::IOBuffer, destr=destr_transient) -> rc

    takes the available bytes from an `IOBuffer` and binds them to a BLOB.

    See also `take!`
"""
@inline function bind(stmt, indx::Integer, io::IOBuffer, destr=destr_transient)
    @boundscheck checkbindbounds(stmt, indx)
    ccall((@s3 bind_blob64), Cint,
          (Ptr{Cvoid},   Cint,      Ptr{UInt8}, UInt64, Ptr{Cvoid}),
          handle(stmt), Cint(indx), io.data,    io.size,     destr)
end
@inline function bind(stmt, indx::Integer, ch::Channel{UInt8}, destr=destr_transient)
    @boundscheck checkbindbounds(stmt, indx)
    ccall((@s3 bind_blob64), Cint,
          (Ptr{Cvoid},   Cint,      Ptr{UInt8}, UInt64,           Ptr{Cvoid}),
          handle(stmt), Cint(indx), ch.data,    ch.n_avail_items, destr)
end

@inline function bind(stmt, indx::Integer, io::IOStream, nb::Integer, destr=destr_transient)
    @boundscheck checkbindbounds(stmt, indx)
    ccall((@s3 bind_blob64), Cint,
          (Ptr{Cvoid},   Cint,      Ptr{UInt8},   UInt64, Ptr{Cvoid}),
          handle(stmt), Cint(indx), read(io, nb), nb,     destr)
end

"""
    bind(::AbstractStmt, indx, ::Float32) -> rc

    binds a single precision Float32:

    if `SINGLE_IS_BLOB[]==true`
        then as a blob, otherwise as a `double`/`Float64`.
"""
@inline function bind(stmt, indx::Integer, val::Float32)
    @boundscheck checkbindbounds(stmt, indx)
    if SINGLE_IS_BLOB[]
        ccall((@s3 bind_blob), Cint,
              (Ptr{Cvoid},   Cint, Ref{Float32}, Clong, Ptr{Cvoid}),
              handle(stmt), indx, val,          4,     Ptr{Nothing}(SQLITE_TRANSIENT))
    else
        bind(stmt, indx, Float64(val))
    end
end

function bind(stmt, indx::Integer, val::AbstractFloat)
    @boundscheck checkbindbounds(stmt, indx)
    if NAN_IS_BLOB[] && !isfinite(val)
        ccall((@s3 bind_blob), Cint,
              (Ptr{Cvoid},   Cint, Ref{Float64}, Clong, Ptr{Cvoid}),
              handle(stmt), indx, val,          8,     Ptr{Nothing}(SQLITE_TRANSIENT))
    else
        ccall((@s3 bind_double), Cint,
              (Ptr{Cvoid},  Cint, Float64),
              handle(stmt), indx, val)
    end
end

@inline function bind(stmt, indx::Integer, val)
    @boundscheck checkbindbounds(stmt, indx)
    io = IOBuffer()
    serialize(io, val)
    bind(stmt, indx, io)
end

bind(stmt::AbstractStmt, str::String) = bind(stmt, 1, str)

function bind(stmt, parnm::String, val)
    indx = bind_parameter_index(stmt, parnm)
    if indx==0
        throw( SqliteError("error", "$parnm is not a parameter in $stmt") )
    end
    @inbounds bind(stmt, indx, val)
end

# bind(stmt, d::AbstractDict) = foreach(p -> bind(stmt, bind_parameter_index(stmt, p.first), p.second), d)

# iterate binding for anything else (assuming it is an Iterator)
"""
    bind(::AbstractStmt, vals)

binds enumerable values "vals" to a prepared statement.
"""
function bind(stmt::AbstractStmt, vals; indx=0)
    foreach(x -> bind(stmt, indx+x[1], x[2]), enumerate(vals))
    stmt
end
# bind(stmt, vals) = foreach(x -> bind(stmt, x[1], x[2]), enumerate(vals))

"""
    bind(::AbstractStmt, ::AbstractDict)

binds values of a Dictionary to named parameters of a statetment, the names are the keys.
"""
bind(stmt, d::AbstractDict) = foreach(p -> bind(stmt, p.first, p.second), d)

@inline function bind(stmt::AbstractStmt, indx::Integer, val::Vector{Float32})
    @boundscheck checkbindbounds(stmt, indx)
    if SINGLE_IS_BLOB[]
        ccall((@s3 bind_blob), Cint,
              (Ptr{Cvoid},  Cint, Ref{Float32}, Clong,         Ptr{Cvoid}),
              handle(stmt), indx, val, length(val)*sizeof(Float32), Ptr{Nothing}(SQLITE_TRANSIENT))
    else
        bind(stmt, indx, Float64.(val))
    end
end

@inline function bind(stmt::AbstractStmt, indx::Integer, val::Vector{T}) where T
    @boundscheck checkbindbounds(stmt, indx)
    ccall((@s3 bind_blob), Cint,
          (Ptr{Cvoid},  Cint, Ref{T}, Clong,         Ptr{Cvoid}),
          handle(stmt), indx, val,    length(val)*sizeof(T), Ptr{Nothing}(SQLITE_TRANSIENT))
end

checked_bind(stmt, indx, val) = bind(stmt, indx, val)!=SQLITE_OK ? throw_stmt(stmt) : stmt

function checked_bind(stmt, a)
    for (k, bvalue) in enumerate(a)
        checked_bind(stmt, k, bvalue)
    end
    stmt
end

checked_bind(stmt, indx, strblob::Union{String, Blob}, destr=Ptr{Nothing}(SQLITE_TRANSIENT)) =
bind(stmt, indx, strblob, destr)!=SQLITE_OK ? throw_stmt(stmt) : stmt

"
Fields is a type for iterating over the fields of a composite type
"
struct Fields{T}
    x::T
end
iterate(f::Fields, state=1) = state>length(fieldnames(f.x)) ? nothing : (getfield(f.x, state), state)
fields(x) = Fields(x)

# test bind function
#bind(stmt::AbstractStmt, indx, x::Union{AbstractFloat,Integer,String,Blob}) = nothing

function bind_zeroblob(stmt, indx, sz)
    if ccall((@s3 bind_zeroblob64), Cint, (Ptr{Cvoid},Cint,Clong),
             handle(stmt), indx, sz)!=SQLITE_OK
        throw_stmt(stmt)
    else
        stmt
    end
end
bind64(stmt, indx, str, destr=Ptr{Nothing}(SQLITE_TRANSIENT)) =
ccall((@s3 bind_text64), Cint,
      (Ptr{Cvoid},   Cint, Ptr{UInt8}, UInt64,      Ptr{Cvoid}, Cint),
      handle(stmt), indx, str,        length(str), destr,      SQLITE_UTF8)

checked_bind64(stmt, indx, str::AbstractString,
               destr=Ptr{Nothing}(SQLITE_TRANSIENT)) =
bind64(stmt, indx, str, destr)!=SQLITE_OK ? throw_stmt(stmt) : stmt

function length(db::DB, bindpar=(); kwargs...)
    cntstmt = prepare_select(db; onlycount=true, kwargs...)
    if !isempty(bindpar)
        bind(cntstmt, bindpar)
    end
    checked_step(cntstmt)
    Int64(cntstmt)
end

"""
    firststep(::AbstractStmt, val)

performs a reset, bind, step sequence typically used to update
"""
function firststep(stmt, val)
    reset(stmt)
    bind(stmt, val)
    step(stmt)
end

function checked_firststep(stmt, val)
    checked_reset(stmt)
    checked_bind(stmt, val)
    checked_step(stmt)
end

function count(cntstmt::AbstractStmt)
    reset(cntstmt)
    step(cntstmt)
    column_int64(cntstmt, 0)
end

"""
    count(::DB, table; where::clause="") -> n

    returns the nr of rows in `table`.
"""
function count(db::DB, tblnm; where="", bindpar=()) 
    cntstmt = prepare_select(db; from=tblnm, onlycount=true, where)
    !isempty(bindpar) && bind(cntstmt, bindpar)
    count(cntstmt)
end

"""
    first(::AbstractStmt, ::Type{T}=Any; bindpar=()) -> first row as a Vector{T}

    default T is Any
"""
function first(stmt::AbstractStmt, ::Type{T}=Any; bindpar=()) where T
    reset(stmt)
    !isempty(bindpar) && bind(stmt, bindpar)
    step(stmt)
    column_count(stmt)==1 ? T(stmt, 0) : [T(stmt, k-1) for k in 1:column_count(stmt)]
end
"""
    first(::DB, ::Type{T}=Any; bindpar=(), selargs, ) -> first row as a Vector{Any}
"""
first(db::DB, ::Type{T}=Any; bindpar=(), selargs...) where T = first(prepare_select(db; selargs...), T; bindpar)

function text(stmt, col=0)
    reset(stmt)
    step(stmt)
    column_string(stmt, col)
end
text(db::DB; selargs...) = text(prepare_select(db; selargs...))

function getindex(stmt::AbstractStmt, bindpar=())
    reset(stmt)
    !isempty(bindpar) && bind(stmt, bindpar)
    step(stmt)
    row(stmt)
end
function getindex(db::DB, key, bindpar=())
    stmt = prepare_select(db; from=key[1], where=key[2])
    getindex(stmt, bindpar)
end

# incremental I/O of blobs
struct BlobHandle <: IO
    handle::Ptr{Cvoid}
    # handlep::Ref{Ptr{Cvoid}}
end
function show(io::IO, bh::BlobHandle)
    if bh.handle == C_NULL
        print(io, "closed")
    else
        print(io, "BlobHandle: $(bytes(bh)) bytes")
    end
end

function openblob(db::DB, tblnm::String, colnm::String, krow::Int, rw::Bool=true;
        dbnm::String="main")
    # sptr = Vector{Ptr{Nothing}}(undef, 1)
    sptr = Ref(C_NULL)
    iret = ccall((@s3 blob_open), Cint,
                 (Ptr{Cvoid}, Ptr{UInt8}, Ptr{UInt8}, Ptr{UInt8}, Int64, Cint, Ptr{Cvoid}),
                 db.handle, dbnm, tblnm, colnm, krow, Int64(rw), sptr)
    iret!=SQLITE_OK && throw( SqliteError(errmsg(db), sql))
    BlobHandle(sptr)
end

reopen(bh::BlobHandle, krow::Int) =
ccall((@s3 blob_reopen), Cint, (Ptr{Cvoid}, Int64), bh.handle, krow)

function close(bh::BlobHandle)
    ccall((@s3 blob_close), Cint, (Ptr{Cvoid},), bh.handle)!=SQLITE_OK &&
    throw( SqliteError(errmsg(db), "when closing $(filename(db)))") )
end

bytes(bh::BlobHandle) = ccall((@s3 blob_bytes), Cint, (Ptr{Cvoid},), bh.handle)

unsafe_write(bh::BlobHandle, z::Ptr{UInt8}, n::UInt, offset::UInt=0) =
    ccall((@s3 blob_write), Cint, (Ptr{Cvoid}, Ptr{UInt8}, Cint, Cint), bh.handle, z, n, offset)

unsafe_read(bh::BlobHandle, z::Ptr{UInt8}, n::UInt, offset::UInt=0) =
    ccall((@s3 blob_read), Cint, (Ptr{Cvoid}, Ptr{UInt8}, Cint, Cint), bh.handle, z, n, offset)

function checked_write(bh::BlobHandle, z::Ptr{UInt8}, n::UInt, offset::UInt=0)
    iret = unsafe_write(bh, z, n, offset)
    iret!=SQLITE_OK && throw( SqliteError(errstr(iret), "when writing blob"))
    return n
end

function checked_read(bh::BlobHandle, z::Ptr{UInt8}, n::UInt, offset::UInt=0)
    iret = unsafe_read(bh, z, n, offset)
    iret!=SQLITE_OK && throw( SqliteError(errstr(iret), "when reading blob"))
    return n
end

#end of incremental blob i/o

data_count(stmt::AbstractStmt) =
    # @threadcall((@s3 data_count), Cint, (Ptr{Cvoid},), handle(stmt))
    ccall((@s3 data_count), Cint, (Ptr{Cvoid},), handle(stmt))

column_count(stmt::AbstractStmt) =
    # @threadcall((@s3 column_count), Cint, (Ptr{Cvoid},), handle(stmt))
    ccall((@s3 column_count), Cint, (Ptr{Cvoid},), handle(stmt))

"""
    column_type(::AbstractStmt) -> 

    1 for 64-bit signed integer
    2 for 64-bit IEEE floating point number
    3 for string
    4 for BLOB
    5 for NULL
"""
function column_type(stmt::AbstractStmt, kcol)
    @boundscheck checkbounds(stmt, kcol)
    # @threadcall((@s3 column_type), Cint, (Ptr{Cvoid},Cint), handle(stmt), Cint(kcol))
    ccall((@s3 column_type), Cint, (Ptr{Cvoid},Cint), handle(stmt), Cint(kcol))
end
column_type(stmt::AbstractStmt, nm::Union{String,Symbol}) = column_type(stmt, colindex(stmt, nm))

function column_name(stmt::AbstractStmt, kcol)
    @boundscheck checkbounds(stmt, kcol)
    unsafe_string(ccall((@s3 column_name), Ptr{UInt8}, (Ptr{Cvoid},Cint),
                        handle(stmt), Cint(kcol)))
end
# column_name(stmt::AbstractStmt, nm::Union{String,Symbol}) = column_name(stmt, colindex(stmt, nm))

function column_bytes(stmt::AbstractStmt, kcol)
    @boundscheck checkbounds(stmt, kcol)
    # @threadcall((@s3 column_bytes), Cint, (Ptr{Cvoid},Cint), handle(stmt), Cint(kcol))
    ccall((@s3 column_bytes), Cint, (Ptr{Cvoid},Cint), handle(stmt), Cint(kcol))
end
column_bytes(stmt::AbstractStmt, nm::Union{String,Symbol}) = column_bytes(stmt, colindex(stmt, nm))

struct ColumnNames{S <: AbstractStmt}
    stmt::S
end
iterate(c::ColumnNames, state=0) =
    state<column_count(c.stmt) ? (column_name(c.stmt, state), state+1) : nothing
eachname(stmt) = ColumnNames(stmt)

struct ColumnKeys{S}
    stmt::S
end
iterate(c::ColumnKeys, state=0) =
    state<column_count(c.stmt) ? (Symbol(column_name(c.stmt, state)), state+1) : nothing
eachkey(stmt) = ColumnKeys(stmt)

struct ColumnValues{S}
    stmt::S
end
iterate(c::ColumnValues, state=0) =
    state<column_count(c.stmt) ? (column(c.stmt, state), state+1) : nothing
eachvalue(stmt) = ColumnValues(stmt)

length(c::Union{ColumnNames, ColumnKeys, ColumnValues}) = column_count(c.stmt)
IteratorSize(::Union{Type{ColumnNames},Type{ColumnKeys},Type{ColumnValues}}) = HasLength()

function column_decltype(stmt::AbstractStmt, kcol::Int)
    @boundscheck checkbounds(stmt, kcol)
    rptr = ccall((@s3 column_decltype), Ptr{UInt8}, (Ptr{Cvoid},Cint), stmt, Cint(kcol))
    return rptr==C_NULL ? "" : unsafe_string(rptr)
end

eachcolnm(stmt::AbstractStmt) = (c for c in ColumnNames(stmt))
function eachcolnm(db::DB, table; start=0, except=(), x=false)
    xstr = x ? "x" : ""
    (column_string(c, 0)
     for (k,c) in enumerate(prepare_select(db; from="pragma_table_$(xstr)info('$table')", columns="name"))
     if k-1>=start && (isempty(except) || column_string(c, 0) ∉ except))
end

# column_names(stmt::AbstractStmt) = [column_name(stmt, k-1) for k in 1:column_count(stmt)]
column_names(stmt::AbstractStmt) = ColumnNames(stmt)

column_names(db::DB, table; kwargs...) = collect(String, eachcolnm(db, table; kwargs...))
# column_names(db::DB, table) =
#     collect(String, prepare(db, "SELECT name FROM pragma_table_info('$table')"))
# function column_names(db::DB, table::String)
#     stmt = prepare_select(db, from="pragma_table_info('$table')")
#     nms = String[]
#     while step(stmt)==SQLITE_ROW
#         push!(nms, column_string(stmt, 1))
#     end
#     return nms
# end

column_list(stmt::AbstractStmt) = join(column_names(stmt), ',')

column_count(db::DB, tblnm) = count(prepare_select(db, from="pragma_table_info('$tblnm')"; onlycount=true))

checkbounds(stmt::AbstractStmt, k) = (k<0 || k>=ncols(stmt)) && throw(BoundsError(stmt, k))

"""
    column_double(stmt, k) -> Float64 value of the 0-based `k`'th column of query `stmt`

    If not already `double`, a conversion according to SQLite rules occurs.
    `k` is bounds checked (omit with `@inbounds`)
"""
@inline function column_double(stmt::AbstractStmt, kcol)
    @boundscheck checkbounds(stmt, kcol)
    ccall((@s3 column_double), Float64, (Ptr{Cvoid},Cint), handle(stmt), kcol)
end
"""
    column_int64(stmt, k) -> Int64 value of the 0-based `k`'th column of query `stmt`

    If not already `long`, a conversion according to SQLite rules occurs.
    `k` is bounds checked (omit with `@inbounds`)
"""
@inline function column_int64(stmt::AbstractStmt, kcol)
    @boundscheck checkbounds(stmt, kcol)
    ccall((@s3 column_int64), Int64, (Ptr{Cvoid},Cint), handle(stmt), kcol)
end
"""
    column_uint64(stmt, k) -> UInt64 value of the 0-based `k`'th column of query `stmt`

    `k` is bounds checked (omit with `@inbounds`)
"""
@inline function column_uint64(stmt::AbstractStmt, kcol)
    @boundscheck checkbounds(stmt, kcol)
    ccall((@s3 column_int64), UInt64, (Ptr{Cvoid},Cint), handle(stmt), kcol)
end
@inline function column_int(stmt::AbstractStmt, kcol)
    @boundscheck checkbounds(stmt, kcol)
    ccall((@s3 column_int), Cint, (Ptr{Cvoid},Cint), handle(stmt), kcol)
end
@inline function column_text(stmt::AbstractStmt, kcol)
    @boundscheck checkbounds(stmt, kcol)
    ccall((@s3 column_text), Ptr{UInt8}, (Ptr{Cvoid},Cint), handle(stmt), kcol)
end
@inline function column_blob(stmt::AbstractStmt, kcol)
    @boundscheck checkbounds(stmt, kcol)
    ccall((@s3 column_blob), Ptr{UInt8}, (Ptr{Cvoid},Cint), handle(stmt), kcol)
end
@inline function column_blob(stmt::AbstractStmt, kcol, ::Type{T}) where T
    @boundscheck checkbounds(stmt, kcol)
    ccall((@s3 column_blob), Ptr{T}, (Ptr{Cvoid},Cint), handle(stmt), kcol)
end
@inline function column_value(stmt::AbstractStmt, kcol)
    @boundscheck checkbounds(stmt, kcol)
    ccall((@s3 column_value), Ptr{Cvoid}, (Ptr{Cvoid},Cint), handle(stmt), kcol)
end

column_int(stmt::AbstractStmt, kcol, ::Type{T}) where T = T(column_int(stmt, kcol))

column_string(stmt::AbstractStmt, kcol) =
    column_type(stmt, kcol)!=SQLITE_NULL ? unsafe_string(column_text(stmt, kcol)) : missing
# column_string(stmt::AbstractStmt, kcol::Int=0) = unsafe_string(column_text(stmt, kcol))

# column_float(stmt::AbstractStmt, kcol) = column_type(stmt, kcol)!=SQLITE_NULL ?
#      column_double(stmt, kcol) : NaN
"""
    column_float(stmt, k) -> Float64 value of column `k`

    Unlike `column_double`, if the database value is NULL, a `NaN` is returned.

    If `DOUBLE_IS_BLOB[]==true`, an 8-byte BLOB representing an IEEE 754 double is expected and returned.

    If `NAN_IS_BLOB[]==true` and the column is an 8-byte BLOB, a IEEE 754 NaN is expected and returned.

    See also [`Litesql.DOUBLE_IS_BLOB[]`](@ref)`, [`Litesql.NAN_IS_BLOB[]`](@ref)`
"""
function column_float(stmt::AbstractStmt, kcol)
    if DOUBLE_IS_BLOB[] || (NAN_IS_BLOB[] && column_type(stmt, kcol)==SQLITE_BLOB)
        if column_bytes(stmt, kcol)==8
            unsafe_load(column_blob(stmt, kcol, Float64))
        else
            SqliteError("column_float $(column_bytes(stmt, kcol)) bytes in blob (8 expected)")
        end
    elseif SINGLE_IS_BLOB[] && column_type(stmt, kcol)==SQLITE_BLOB
        if column_bytes(stmt, kcol)==4
            unsafe_load(column_blob(stmt, kcol, Float32))
        else
            SqliteError("column_float $(column_bytes(stmt, kcol)) bytes in blob (4 expected)")
        end
    else
        return column_type(stmt, kcol)!=SQLITE_NULL ? column_double(stmt, kcol) : NaN
    end
end
column_float(stmt::AbstractStmt, kcol, ::Type{T}) where T = T(column_float(stmt, kcol))

column_blob(stmt::AbstractStmt, kcol, io::IO) =
    unsafe_write(io, column_blob(stmt, kcol), column_bytes(stmt, kcol))

column_double(stmt::AbstractStmt, colidx::AbstractArray=eachindex(stmt)) =
    Float64[column_double(stmt, k) for k in colidx]
column_int(stmt::AbstractStmt, colidx::AbstractArray=eachindex(stmt)) =
    Int32[column_int(stmt, k) for k in colidx]
column_int64(stmt::AbstractStmt, colidx::AbstractArray=eachindex(stmt)) =
    Int64[column_int64(stmt, k) for k in colidx]
column_uint64(stmt::AbstractStmt, colidx::AbstractArray)=eachindex(stmt) =
    Int64[column_uint64(stmt, k) for k in colidx]
column_string(stmt::AbstractStmt, colidx::AbstractArray=eachindex(stmt)) =
    String[column_string(stmt, k) for k in colidx]

column_blobcopy(stmt::AbstractStmt, kcol) =
    UInt8[unsafe_load(column_blob(stmt, kcol), k) for k in 1:column_bytes(stmt, kcol)]

"""
    column_single(stmt, k) -> Float32 value
"""
function column_single(stmt::AbstractStmt, kcol=0)
    if SINGLE_IS_BLOB[]
        if column_bytes(stmt, kcol)==4
            @debug "loading 4 bytes as Float32"
            # unsafe_load(Ptr{Float32}(column_blob(stmt, kcol)))
            unsafe_load(column_blob(stmt, kcol, Float32))
        else 
            SqliteError("column_single: $(column_bytes(stmt, kcol)) bytes in blob (4 expected)")
        end
    else
        Float32(column_double(stmt, kcol))
    end
end

function column_single(stmt::AbstractStmt, kcol, bindx)
    if column_bytes(stmt, kcol)>=bindx*4
        unsafe_load(Ptr{Float32}(column_blob(stmt, kcol)), bindx)
    else 
        SqliteError("column_single: $(column_bytes(stmt, kcol)) bytes in blob (>=$(4*bindx) expected)")
    end
end
"""
    column_float32(stmt, k) -> Float32 value, for database NULL a `NaN` is returned
"""
column_float32(stmt::AbstractStmt, kcol=0) =
column_type(stmt, kcol)!=SQLITE_NULL ? column_single(stmt, kcol) : NaN32
column_float32(stmt::AbstractStmt, kcol, bindx) =
column_type(stmt, kcol)!=SQLITE_NULL ? column_single(stmt, kcol, bindx) : NaN32
function checked_column_float32(stmt::AbstractStmt, kcol=0)
    if column_type(stmt, kcol)!=SQLITE_NULL
        column_single(stmt, kcol)
    else
        NaN32
    end
end
function checked_column_float32(stmt::AbstractStmt, kcol, bindx)
    if column_type(stmt, kcol)!=SQLITE_NULL
        nb = column_bytes(stmt, kcol)
        if mod(nb, 4)==0 && nb>=bindx*4
            column_single(stmt, kcol, bindx)
        else 
            SqliteError("column_float32: $(column_bytes(stmt, kcol)) bytes in blob"*
                        " does not match expectations")
        end
    else
        NaN32
    end
end

column_doubleblob(stmt, k=0) = unsafe_load(Ptr{Float64}(column_blob(stmt, k)))
column_doubleblob(stmt, k, bindx) = unsafe_load(Ptr{Float64}(column_blob(stmt, k)), bindx)
column_float64(stmt, k=0) =
    column_type(stmt, k)!=SQLITE_NULL ? column_doubleblob(stmt, k) : NaN
column_float64(stmt, k, bindx) =
    column_type(stmt, k)!=SQLITE_NULL ? column_doubleblob(stmt, k, bindx) : NaN
function checked_column_float64(stmt, k=0)
    if column_type(stmt, k)!=SQLITE_NULL
        if column_bytes(stmt, k)==8
            column_double(stmt, k)
        else 
            SqliteError("column_float64: $(column_bytes(stmt, k)) bytes in blob (8 expected)")
        end
    else
        NaN
    end
end

function checksizes(stmt, n, ::Type{T}, kcol) where T
    if column_bytes(stmt, kcol)!=sizeof(T)*n
        BoundsError(stmt, kcol)
    end
end

@inline function column_vector(stmt::AbstractStmt, n, ::Type{T}, kcol=0) where T
    @boundscheck checksizes(stmt, n, T, kcol)
    r = Vector{T}(undef, n)
    unsafe_copyto!(pointer(r), Ptr{T}(column_blob(stmt, kcol)), n)
    r
end

# Returning Nullables
function nullable(stmt::AbstractStmt, ::Type{T}, idx::Union{Int,String,Symbol}) where T
    column_type(stmt, idx)==SQLITE_NULL ? missing : T(stmt, idx)
end
# nullable_double(stmt::AbstractStmt, kcol) =
#     column_type(stmt, kcol)==SQLITE_NULL ? missing : column_double(stmt, kcol)
# nullable_int(stmt::AbstractStmt, kcol) = 
#     column_type(stmt, kcol)==SQLITE_NULL ? missing : column_int(stmt, kcol)    
# nullable_int64(stmt::AbstractStmt, kcol) = 
#     column_type(stmt, kcol)==SQLITE_NULL ? missing : column_int64(stmt, kcol)
# nullable_string(stmt::AbstractStmt, kcol) =
#     column_type(stmt, kcol)==SQLITE_NULL ? missing : column_string(stmt, kcol)
# nullable_blob(stmt::AbstractStmt, kcol) =
#     column_type(stmt, kcol)==SQLITE_NULL ? missing : column_blobcopy(stmt, kcol)

"""
    column_object(stmt, k) -> deserialized Julia object

    which previously has been inserted with bind(insstmt, kcol, val)
"""
column_object(stmt::AbstractStmt, kcol) =
    column_bytes(stmt, kcol)>8 && unsafe_string(column_blob(stmt, kcol, UInt8), 3)=="7JL" ?
        deserialize(IOBuffer(column_blobcopy(stmt, kcol))) : column_blobcopy(stmt, kcol)
column_null(stmt::AbstractStmt, kcol) = missing

const colfunc = (column_int64, column_double, column_string, column_object, column_null)

# function column(stmt::AbstractStmt, kcol)
#     coltype = column_type(stmt, kcol)
#     if !(1 <= coltype <= 5)
#         error("sqlite3_column_type not between 1 and 5!")
#     end
#     colfunc[coltype](stmt, kcol)
# end
column(stmt::AbstractStmt, kcol) = colfunc[column_type(stmt, kcol)](stmt, kcol)

"""
    getindex(stmt::AbstractStmt, k) -> return the value of the k-th column selected by `stmt`

`k` can be an integer index, a string or a symbol. `getindex` is not type-stable,
because rows in SQLite tables are not type-stable. 
"""
getindex(stmt::AbstractStmt, colidx::Integer) = column(stmt, colidx)
getindex(stmt::AbstractStmt, colnm::String) = column(stmt, colindex(stmt, colnm))
getindex(stmt::AbstractStmt, colsym::Symbol) = column(stmt, colindex(stmt, String(colsym)))

"""
    row(::AbstractStmt) -> the current row in a named tuple 

"""
row(stmt::AbstractStmt) =  (; zip(eachkey(stmt), eachvalue(stmt))...)
# row(stmt::AbstractStmt) =  (; zip(collect(eachkey(stmt))[collect(values(stmt))], collect(1:ncols(stmt)))...)
function row(stmt::AbstractStmt, cols)
    keys = [Symbol(column_name(stmt, k)) for k in cols]
    vals = [column(stmt, k) for k in cols]
    (; zip(keys, vals)...)
end
function row(stmt::AbstractStmt, func::Function)
    keys = [Symbol(column_name(stmt, k)) for k in cols]
    vals = [func(stmt, k) for k in cols]
    (; zip(keys, vals)...)
end

function colindex(stmt::AutoStmt, nm::String)
    for k in 0:column_count(stmt)-1
        if column_name(stmt, k)==nm
            return k
        end
    end
    SqliteError("no column \"$nm\" in statement $(sql(stmt))")
end
colindex(stmt::AutoStmt, nm::Symbol) = colindex(stmt, String(nm))

struct Statement{S} <: AbstractStmt
    stmt::S
    cols::Dict{Symbol, Int}
end
"""
Statement(stmt::AbstractStmt, cols=1:column_count(stmt)) -> high level Statement

Column names are mapped to integer indices. Optionally only a subset
of columns can be used in the Statement.

Julia is here much faster than SQLite's `column_index`.
"""
Statement(stmt, cols=1:column_count(stmt)) =
Statement(stmt,  Dict((Symbol(column_name(stmt, k-1)), k-1) for k in cols))
# function Statement(stmt, cols=1:column_count(stmt))
#     @show cols
#     @show Dict((Symbol(column_name(stmt, k-1)), k-1) for k in cols)
#     Statement(stmt,  Dict((Symbol(column_name(stmt, k-1)), k-1) for k in cols))
# end
show(io::IO, s::Statement) = show(io, "$(AutoStmt(handle(s)))\n$(s.cols)")
ncols(s::Statement)     = s.stmt.ncols
nbindpars(s::Statement) = s.stmt.nbindpars
handle(s::Statement)  = handle(s.stmt)
eachkey(s::Statement) = keys(s.cols)

# step,reset and bind already calls handle
# step(::Statement) = step ∘ handle
# reset(::Statement) = reset ∘ handle
# bind(s::Statement) = bind ∘ handle

# column_name(s::Union{StmtLen,Statement}) = column_name(s.stmt)

colindex(stmt::Statement, nm) = stmt.cols[nm]

colindex(stmt::Statement, nms::String) = colindex(stmt, Symbol(nms))

row(s::Statement, func::Function) = (; zip(keys(s.cols), [func(s, k) for k in values(s.cols)])...)

const SS = Union{String,Symbol}
column_double(stmt::AbstractStmt, nm::SS) = column_double(stmt, colindex(stmt, nm))
column_int64(stmt::AbstractStmt, nm::SS) = column_int64(stmt, colindex(stmt, nm))
column_uint64(stmt::AbstractStmt, nm::SS) = column_uint64(stmt, colindex(stmt, nm))
column_int(stmt::AbstractStmt, nm::SS) = column_int(stmt, colindex(stmt, nm))
column_text(stmt::AbstractStmt, nm::SS) = column_text(stmt, colindex(stmt, nm))
column_blob(stmt::AbstractStmt, nm::SS) = column_blob(stmt, colindex(stmt, nm))
column_value(stmt::AbstractStmt, nm::SS) = column_value(stmt, colindex(stmt, nm))

column_string(stmt::AbstractStmt, nm::SS) = column_string(stmt, colindex(stmt, nm))
column_ascii(stmt::AbstractStmt, nm::SS) = column_ascii(stmt, colindex(stmt, nm))

column_float(stmt::AbstractStmt, nm::SS) = column_float(stmt, colindex(stmt, nm))
column_float32(stmt::AbstractStmt, nm::SS) = column_float32(stmt, colindex(stmt, nm))

column(stmt::AbstractStmt, nm::SS) = column(stmt, colindex(stmt, nm))

"""
    iterate(::AbstractStmt, state) enables to iterate over a prepared statements Use like

    for _s in stmt
        x = column_float(_s, 0)
        ...
    end
"""
iterate(stmt::AbstractStmt, state=SQLITE_OK) = (state=step(stmt))==SQLITE_ROW ? (stmt,state) : nothing
IteratorSize(::Type{T}) where {T<:AbstractStmt}= SizeUnknown()
eltype(::Type{AbstractStmt}) = AbstractStmt
# eltype(::Type{AbstractStmt}) = Vector{Any}

# column_double(s::Statement, nm::String)   = column_double(s, s.col[nm])
# column_float(s::Statement, nm::String)    = column_float(s, s.col[nm])
# column_int64(s::Statement, nm::String)    = column_int64(s, s.col[nm])
# column_uint64(s::Statement, nm::String)   = column_uint64(s, s.col[nm])
# column_int(s::Statement, nm::String)      = column_int(s, s.col[nm])
# column_string(s::Statement, nm::String)   = column_string(s, s.col[nm])
# column_ascii(s::Statement, nm::String)    = column_ascii(s, s.col[nm])
# column_utf8(s::Statement, nm::String)     = column_utf8(s, s.col[nm])
# column_text(s::Statement, nm::String)     = column_text(s, s.col[nm])
# column_blob(s::Statement, nm::String)     = column_blob(s, s.col[nm])
# column_blobcopy(s::Statement, nm::String) = column_blobcopy(s, s.col[nm])
# column_value(s::Statement, nm::String)    = column_value(s, s.col[nm])
# 
# column(s::Statement, nm::String) = column(s.stmt, s.col[nm])


Any(stmt::AbstractStmt, idx=0) = column(stmt, idx)
(::Type{Vector{Any}})(stmt::AbstractStmt, rng=0:column_count(stmt)-1) =
    [column(stmt, kc) for kc in rng]
(::Type{Vector{Any}})(stmt::AbstractStmt, cols::String) =
    [column(stmt, strip(nm)) for nm in split(cols, ',')]

Float64(stmt::AbstractStmt, idx=0) = column_float(stmt, idx)
(::Type{Vector{Float64}})(stmt::AbstractStmt, rng=0:column_count(stmt)-1) =
    [column_float(stmt, kc) for kc in rng]
(::Type{Vector{Float64}})(stmt::AbstractStmt, cols::String) =
    [column_float(stmt, strip(nm)) for nm in split(cols, ',')]

Int64(stmt::AbstractStmt, idx=0) = column_int64(stmt, idx)
(::Type{Vector{Int64}})(stmt::AbstractStmt, rng=0:column_count(stmt)-1) =
    [column_int64(stmt, kc) for kc in rng]
(::Type{Vector{Int64}})(stmt::AbstractStmt, cols::String) =
    [column_int64(stmt, strip(nm)) for nm in split(cols, ',')]

Int32(stmt::AbstractStmt, idx=0) = column_int(stmt, idx)
(::Type{Vector{Int32}})(stmt::AbstractStmt, rng=0:column_count(stmt)-1) =
    [column_int(stmt, kc) for kc in rng]
(::Type{Vector{Int32}})(stmt::AbstractStmt, cols::String) =
    [column_int(stmt, strip(nm)) for nm in split(cols, ',')]

(::Type{Vector{T}})(stmt::AbstractStmt, n, kcol) where T = column_vector(stmt, n, T, kcol)

# Int16(stmt::AbstractStmt, idx=0) = Int16(column_int(stmt, idx))
# (::Type{Vector{Int16}})(stmt::AbstractStmt, rng=0:column_count(stmt)-1) =
#     [Int16(column_int(stmt, kc)) for kc in rng]
# (::Type{Vector{Int16}})(stmt::AbstractStmt, cols::String) =
#     [Int16(column_int(stmt, strip(nm))) for nm in split(cols, ',')]

String(stmt::AbstractStmt, idx=0) = column_string(stmt, idx)
(::Type{Vector{String}})(stmt::AbstractStmt, rng=0:column_count(stmt)-1) =
    [column_string(stmt, kc) for kc in rng]
(::Type{Vector{String}})(stmt::AbstractStmt, cols::String) =
    [column_string(s, strip(nm)) for nm in split(cols, ',')]

Blob(stmt::AbstractStmt, idx=0) = column_blobcopy(stmt, idx)
(::Type{Vector{Blob}})(stmt::AbstractStmt, rng=0:column_count(stmt)-1) =
    [column_blobcopy(stmt, kc) for kc in rng]
(::Type{Vector{Blob}})(stmt::AbstractStmt, cols::String) =
    [column_blobcopy(s, strip(nm)) for nm in split(cols, ',')]

Float32(stmt::AbstractStmt, idx=0) = column_float32(stmt, idx)
(::Type{Vector{Float32}})(stmt::AbstractStmt, rng=0:column_count(stmt)-1) =
    [column_float32(stmt, kc) for kc in rng]
(::Type{Vector{Float32}})(stmt::AbstractStmt, cols::String) =
    [column_float32(stmt, strip(nm)) for nm in split(cols, ',')]

UInt64(stmt::AbstractStmt, idx=0) = column_int64(stmt, idx)
(::Type{Vector{UInt64}})(stmt::AbstractStmt, rng=0:column_count(stmt)-1) =
    [column_int64(stmt, kc) for kc in rng]
(::Type{Vector{UInt64}})(stmt::AbstractStmt, cols::String) =
    [column_int64(stmt, strip(nm)) for nm in split(cols, ',')]

Bool(stmt::AbstractStmt, idx=0) = column_int64(stmt, idx)!=0

NTuple{N, Float64}(stmt::AbstractStmt, colbeg=0) where N =
    NTuple{N, Float64}(column_float(stmt, k) for k in colbeg .+ (0:N-1))
NTuple{N, Float32}(stmt::AbstractStmt, colbeg=0) where N =
    NTuple{N, Float32}(column_float32(stmt, k) for k in colbeg .+ (0:N-1))
NTuple{N, T}(stmt::AbstractStmt, colbeg=0) where {N,T<:Union{Int8,Int16,Int32}} =
    NTuple{N, T}(column_int(stmt, k) for k in colbeg .+ (0:N-1))
NTuple{N, String}(stmt::AbstractStmt, colbeg=0) where N =
    NTuple{N, String}(column_string(stmt, k) for k in colbeg .+ (0:N-1))
NTuple{N, Blob}(stmt::AbstractStmt, colbeg=0) where N =
    NTuple{N, Blob}(column_blobcopy(stmt, k) for k in colbeg .+ (0:N-1))

Tuple(stmt::AbstractStmt) = (column(stmt, k) for k in eachindex(stmt))
# Tuple(stmt::AbstractStmt) = (column(stmt, k) for k in 0:ncols(stmt)-1)

column(stmt::AbstractStmt, ::Type{T}, idx::Union{Int,String,Symbol}) where T = T(stmt, idx)
column(stmt::AbstractStmt, f::F, idx::Union{Int,String,Symbol}) where F<:Function = f(stmt, idx)

# column(stmt::AbstractStmt, ::Type{Float64}, idx::Union{Int,String,Symbol}) = column_float(stmt, idx)
# column(stmt::AbstractStmt, ::Type{Int}, idx::Union{Int,String,Symbol}) = column_int64(stmt, idx)
# column(stmt::AbstractStmt, ::Type{String}, idx::Union{Int,String,Symbol}) = column_string(stmt, idx)
# column(stmt::AbstractStmt, ::Type{Blob}, idx::Union{Int,String,Symbol}) = column_blobcopy(stmt, idx)
# column(stmt::AbstractStmt, ::Type{Float32}, idx::Union{Int,String,Symbol}) = column_float32(stmt, idx)

"Sqliter is an abstract supertype for iterating over selected rows"
abstract type Sqliter end

# iterate(s::Sqliter) = iterate(s, reset(s.stmt)) 
# iterate(s::Sqliter) = iterate(s, SQLITE_OK) 
function iterate(r::R, state=SQLITE_OK) where {R<:Sqliter}
    state = step(r.stmt)
    if state==SQLITE_ROW
        return row(r),state
        # return r(),state
    else
        state!=SQLITE_DONE && throw( StmtError(r.stmt, state) )
        # r.flz && finalize(r.stmt)
        return nothing
    end
end

# Iterators.IteratorSize(::R) where {R<:Sqliter} = SizeUnknown()
Iterators.IteratorSize(::Type{Sqliter}) = SizeUnknown()
# eltype(::Type{Sqliter}) = Vector{Any}
eltype(r::Sqliter) = Tuple
function show(io::IO, s::Sqliter)
    for _it in Iterators.take(s, 5)
        show(io, _it)
    end
    for _l=1:3
        print(io, ".\n")
    end
    reset(s.stmt)
end

" Rowit(stmt::AbstractStmt, RType::Type) -> iterator over database rows returned by a SELECT.
At each iteration a constructor RType(stmt::AbstractStmt) is called"
struct Rowit{S<:AbstractStmt, RType} <: Sqliter
    stmt::S
    RType::Type{RType}
    # flz::Bool
end
# Rowit(stmt::AbstractStmt, RType::Type; finize::Bool=false) = Rowit(stmt, RType, finize)
row(r::Rowit) = r.RType(r.stmt)
(r::Rowit)() = r.RType(r.stmt)

" Rows(stmt::AbstractStmt, rowfun::Function, args) -> iterator over selected database rows
A function rowfun(stmt, args...) is called at each iteration. Use for loading database
rows into Julia vectors"
struct RowsFun{S<:AbstractStmt, F<:Function} <: Sqliter
    stmt::S
    rowfun::F
    args::Tuple
    # flz::Bool
end
row(r::RowsFun) = r.rowfun(r.stmt, r.args...)
(r::RowsFun)() = r.rowfun(r.stmt, r.args...)

" Rowith(stmt::AbstractStmt, RType::Type, handover::H) -> iterator,
iterates over database rows and hands something over, complementing the Julia object
with constant data. A constructor RType(stmt, handover) is called at each iteration"
struct Rowith{S<:AbstractStmt, RType, H} <: Sqliter
    stmt::S
    RType::Type{RType}
    handover::H
    # flz::Bool
end
# Rowith(stmt::AbstractStmt, RType::Type, handover; finize::Bool=false) =
#     Rowith(stmt, RType, handover, finize)
row(r::Rowith) = r.RType(r.stmt, r.handover)
(r::Rowith)() = r.RType(r.stmt, r.handover)

" Rowiths(stmt::AbstractStmt, RType::Type, handovers::Tuple) -> iterator,
iterates over database rows and hands things over, complementing the Julia object
with constant data. A constructor RType(stmt, handovers...) is called at each iteration."
struct Rowiths{S<:AbstractStmt, RType} <: Sqliter
    stmt::S
    RType::Type{RType}
    handovers::Tuple
    # flz::Bool
end
# Rowiths(stmt::AbstractStmt, ::Type{T}, handovers::Tuple; finize::Bool=false) where T =
#     Rowiths(stmt, T, handovers, finize)
row(r::Rowiths) = r.RType(r.stmt, r.handovers...)
(r::Rowiths)() = r.RType(r.stmt, r.handovers...)

struct Row{S<:AbstractStmt} <: Sqliter
    stmt::S
end
row(r::Row) = rowtuple(r.stmt)

eltype(r::Union{Type{Rowit}, Type{Rowith}, Type{Rowiths}}) = r.RType

# Iterators over db rows:

"""
    rows(stmt, ...) -> Iterator

    The `rows` function returns an iterator stepping over the rows of a SQLite statement.

    Example:

    lat_rad = []
    stmt    = prepare_select(db; from="locations", columns="latitude")
    foreach( rows(stmt, column_double, (0,)) ) do lat
        push!(lat_rad, deg2rad(lat))
    end
"""
function rows end
"""
    rows(stmt::AbstractStmt) -> Iterator over the rows selected with the prepared statement `stmt`

At the each iteration the row is returned as a tuple.
The method is not type-stable. The start iteration does not call `reset`.
"""
rows(stmt::AbstractStmt) = Row(stmt)
# Enable to pipe a 'stmt' into rows (and other functions taking 'stmt' as the only argument):
|>(stmt::AbstractStmt, func::Function) = func(stmt)
"""
    rows(stmt::AbstractStmt, T::Type{T}) -> Iterator over the rows selected with the prepared statement `stmt`

At the each iteration the constructor `T(stmt)` is called and the resulting row object returned.
The start iteration does not call `reset`.
"""
rows(stmt::AbstractStmt, ::Type{T}) where T = Rowit(stmt, T)
"""
    rows(stmt::AbstractStmt, T::Type{T}, h) -> Iterator

hands at each iteration `h` over to the constructor, `T(stmt, h)`.
"""
rows(stmt::AbstractStmt, ::Type{T}, h) where T = Rowith(stmt, T, h)
"""
    rows(stmt::AbstractStmt, T::Type{T}, h::Tuple) -> Iterator

hands at each iteration the splatted `h` over to the constructor, `T(stmt, h...)`.
"""
rows(stmt::AbstractStmt, ::Type{T}, h::Tuple) where T = Rowiths(stmt, T, h)
"""
    rows(stmt::AbstractStmt, func::Function, args=()) -> Iterator

At the each iteration the function `func(stmt, args...)` is called and the resulting row object returned.
"""
rows(stmt::AbstractStmt, func::Function, args=()) = RowsFun(stmt, func, args)

"""
    eachrow(stmt::AbstractStmt)

Create a generator that iterates over the row tuples of a db table.

Example:
========

julia> db = opendb(); julia> exec(db, "CREATE TABLE locations(id INTEGER PRIMARY KEY, name TEXT, latitude REAL, longitude REAL);")
0

julia> insstmt = prepare_insert(db, into="locations")
SQL: INSERT INTO locations VALUES (?1,?2,?3,?4);

julia> (nothing, "Uppsala", 59.86, 17.63) |> insstmt
101

julia> (nothing, "Kiruna", 67.85, 20.23)  |> insstmt
101

julia> stmt=prepare_select(db, from="locations", columns="latitude,longitude")
SQL: SELECT latitude,longitude FROM locations;

julia> first(eachrow(stmt))
(59.86, 17.63)

julia> reset(stmt)
0

julia> collect(eachrow(stmt))
2-element Vector{Tuple{Float64, Float64}}:
 (59.86, 17.63)
 (67.85, 20.23)

"""
# eachrow(stmt::AbstractStmt) = (rowtuple(s) for s in stmt) 
eachrow(stmt::AbstractStmt, cols=1:ncols(stmt)) = (rowtuple(s, cols) for s in stmt) 

"""
    eachrow(stmt::AbstractStmt, ::Type)

Create a generator that iterates over the rows of a db table returning the row converted to type `T`.
A constructor for `T` from the statement `stmt` is called.
"""

eachrow(stmt::AbstractStmt, ::Type{T}) where T = (T(s) for s in stmt)
"""
    eachtuple(stmt::AbstractStmt, T::Type)

Create a generator that iterates over the rows of a db table
and returns a tuple for each row where the elements are of type `T`.

"""
eachtuple(stmt::AbstractStmt, ::Type{T}) where T = (tuple(s, T) for s in stmt)

"""
    eachvec(stmt::AbstractStmt)

Create a generator that iterates over the row vectors of a db table.
"""
eachvec(stmt::AbstractStmt) = (rowvec(s) for s in stmt) 
eachvec(stmt::AbstractStmt, ::Type{T}) where T = (rowvec(s, T) for s in stmt) 

# extract a single column from a statement
struct Values{S<:AbstractStmt, FUN<:Function}
    stmt::S
    kcol::Int
    colfun::FUN
end
iterate(v::Values, state=SQLITE_OK) =
    (state=step(v.stmt))==SQLITE_ROW ? (v.colfun(v.stmt, v.icol),state) : nothing
eachrow(stmt::AbstractStmt, ::Type{T}, kcol) where T<:AbstractFloat =
    (T(s) for s in Values(stmt, kcol, column_float))
eachrow(stmt::AbstractStmt, ::Type{T}, kcol) where T<:Integer =
    (T(s) for s in Values(stmt, kcol, column_int64))
eachrow(stmt::AbstractStmt, ::Type{String}, kcol) = (s for s in Values(stmt, kcol, column_string))
eachrow(stmt::AbstractStmt, ::Type{Blob}, kcol) = (func(s) for s in Values(stmt, kcol, column_blobcopy))

eachrow(stmt::AbstractStmt, func::Function, args=()) = (func(s, args...) for s in stmt)

# Channels
# channel(stmt::AbstractStmt, ::Type{T}; size=32) where T = Channel{T}(size) do ch
#     foreach(row -> put!(ch, row), rows(stmt, T))
# end
channel(stmt::AbstractStmt; size=32) = Channel{Tuple}(size) do ch
    foreach(row -> put!(ch, row), rows(stmt))
end
channel(stmt::AbstractStmt, ::Type{T}; size=32) where T = Channel{T}(size) do ch
    foreach(row -> put!(ch, row), rows(stmt, T))
end
channel(stmt::AbstractStmt, ::Type{T}, h; size=32) where T = Channel{T}(size) do ch
    foreach(row -> put!(ch, row), rows(stmt, T, h))
end

function insert(stmt::AbstractStmt, ch::Channel)
    for x in ch
        bind(stmt, x)
        step(stmt)
    end
end
function transact(stmt::AbstractStmt, ch::Channel)
    db = db(stmt)
    transaction(db)
    insert(stmt, ch)
    commit(db)
end

# Functions returning database rows as Julia vectors
"""
    rowvec(::AbstractStmt[, cols]) -> row as a Vector{Any}

`step` must be called first to get a new row. The method is not type-stable.
"""
rowvec(stmt, cols=1:column_count(stmt)) = [column(stmt, k-1) for k in cols]
# specify element type, effectively same as T[]
"""
    rowvec(::AbstractStmt, ::Type{T}[, cols]) -> row as a Vector{T}

The method is type-stable, if `T<:Union{AbstractStmt,Integer,AbstractString,Vector{UInt64}}`,
but conversion from the original datatype may occur.
"""
rowvec(stmt, ::Type{T}, cols=1:column_count(stmt)) where T =
    [T(column(stmt, k-1)) for k in cols]
rowvec(stmt, ::Type{T}, cols=1:column_count(stmt)) where T<:AbstractFloat =
    T[column_float(stmt, k-1) for k in cols]
rowvec(stmt, ::Type{T}, cols=1:column_count(stmt)) where T<:Integer =
    T[column_int64(stmt, k-1) for k in cols]
rowvec(stmt, ::Type{AbstractString}, cols=1:column_count(stmt)) =
    String[column_string(stmt, k-1) for k in cols]
rowvec(stmt, ::Type{Blob}, cols=1:column_count(stmt)) = Blob[column_blobcopy(stmt, k-1) for k in cols]

"""
    rowtuple(::AbstractStmt) -> row as a Tuple

`step` must be called first to get a new row. The method is not type-stable.
"""
# rowtuple(stmt::AbstractStmt) = ntuple(k -> column(stmt, k-1), ncols(stmt))
rowtuple(stmt::AbstractStmt, cols=1:ncols(stmt)) = ntuple(k -> column(stmt, cols[k]-1), length(cols))
# function rowtuple(stmt::AbstractStmt)
#     r = ntuple(k -> column(stmt, k-1), ncols(stmt))
#     if ncols(stmt)>1
#         println("rowtuple: column_type(stmt, 0) = $(column_type(stmt, 0)), column_type(stmt, 1) = $(column_type(stmt, 1))")
#     end
#     println("rowtuple = $r")
#     r
# end
"""
    rowtuple(::AbstractStmt, ::Type{T}) -> row as a Tuple with elements of type T

The method is type-stable, a conversion from the original datatype in the SQLite table may occur.
"""
rowtuple(stmt::AbstractStmt, ::Type{T}, cols=1:ncols(stmt)) where T<:AbstractFloat =
    ntuple(k -> T(column_float(stmt, cols[k]-1)), length(cols))
rowtuple(stmt::AbstractStmt, ::Type{T}, cols=1:ncols(stmt)) where T<:Integer =
    ntuple(k -> T(column_int64(stmt, cols[k]-1)), length(cols))
rowtuple(stmt::AbstractStmt, ::Type{AbstractString}, cols=1:ncols(stmt)) =
    ntuple(k -> column_string(stmt, cols[k]-1), length(cols))
rowtuple(stmt::AbstractStmt, ::Type{Blob}, cols=1:ncols(stmt)) =
    ntuple(k -> column_blobcopy(stmt, cols[k]-1), length(cols))

# execute a statement ignoring any results
# exec(stmt::AbstractStmt) = check(step(stmt), stmt)
# exec(db::DB, sqltxt::String) = exec(prepare(db, sqltxt, AutoStmt))
exec(db::DB, sqltxt) =
    ccall((@s3 exec), Cint,
          (Ptr{Cvoid}, Ptr{UInt8}, Ptr{Cvoid}, Ptr{Cvoid}, Ptr{Cvoid}),
          db.handle, sqltxt, C_NULL, C_NULL, C_NULL)  
# function exec(db::DB, sqltxt)
#     iret = ccall((@s3 exec), Cint, (Ptr{Cvoid}, Ptr{UInt8}, Ptr{Cvoid}, Ptr{Cvoid}, Ptr{Cvoid}),
#                              db.handle, sqltxt, C_NULL, C_NULL, C_NULL)  
#     iret==SQLITE_OK ? nothing : iret
# end
function checked_exec(db::DB, sqltxt)
    iret = exec(db, sqltxt)
    iret!=SQLITE_OK && error("exec returned $iret\n$(errstr(iret))")
end

function run(db::DB, io::IO)
    sqlstr = read(io, String)
    while true
        stmt,tailstr = prepare_m(db, sqlstr)
        @debug stmt tailstr
        stmt.handle==C_NULL && break
        checked_step(stmt)
        sqlstr = tailstr
        finalize!(stmt)
    end
end
# run(dbnm::AbstractString, io::IO) = run(opendb(dbnm, "w"), io)
run(dbnm::AbstractString, io::IO) = opendb(dbnm, "w") do io
    run(io)
end

"""
  ntuple(stmt::AbstractStmt, f::Function, n)

  From a row in the SQLite db, create a tuple of length n,
  computing each element as f(stmt, k), where k is the index of the element.
"""
ntuple(stmt::AbstractStmt, f::F, n::Integer) where F = ntuple(k -> f(stmt, k), n)
ntuple(stmt::AbstractStmt, f::F, ::Val{N}) where {F,N} = ntuple(k -> f(stmt, k), Val(N))

collect(::Type{R}, stmt::AbstractStmt) where R = [R(it) for it in stmt]
collect(stmt::AbstractStmt, ::Type{R}, ho...) where R = [R(it, ho...) for it in stmt]

"""
select(stmt::AbstractStmt) -> vector of selected rows, each row/vector element a tuple

returns a faithful repesentation of the SQLite table data in Julia memory. The function lacks
type stability. In SQLite each value has its own typem and the type in each column 
can change from row to row. Therefore the row/vector element can have different Tuple types.  
"""
function select(stmt; onlycount=false, withcount=false)::Vector{Tuple}
    onlycount && return count(stmt)

    if withcount
        ra = Vector{Tuple}(undef, Int(stmt[2], 0))
        @sync for (k, tpl) in enumerate(rows(stmt[1]))
            @async ra[k] = tpl
        end
    else
        ra = Tuple[]
        @sync for it in rows(stmt)
            @async push!(ra, it)
        end
    end
    ra
end
# """
# select(stmt::AbstractStmt, ::Type{T}) ->
#     vector of rows, each row a tuple where all elements have type T 
# 
# Conversions to the SQLite types, Float64 aka REAL, Integer, String aka TEXT, and Vector{UInt8} aka BLOB,
# is done by the SQLite library, in some cases followed by a conversion by Julia.
# 
# Examples:
# 
# * `select(stmt, String)` returns a column value of 12+1/3 as
# "12.3333333333333", as converted by SQLite (Julia would print 18 digits). 
# 
# * `select(stmt, Float32)` returns a column value of "3.14" as 3.14f0:
# SQLite parses to double precision 3.14 (the only float type that SQLite
# handles), then Julia converts the Float64 to Float32.
# """
# function select(stmt, ::Type{T}) where T
#     ra = Tuple[]
#     @sync for _it in rows(stmt, rowtuple, (T,))
#         @async push!(ra, _it)
#     end
#     ra
# end
"""
select(stmt::AbstractStmt, ::Type{T}) ->
    vector of rows, each row has type T 
"""
function select(stmt, ::Type{T}) where T
    ra = T[]
    @sync for _it in rows(stmt, T)
        @async push!(ra, _it)
    end
    ra
end

function select2tuples(stmt, ::Type{T}) where T
    # ra = NTuple{T, ncols(stmt)}[]
    ra = Tuple[]
    @sync for _it in rows(stmt, rowtuple, (T,))
        @async push!(ra, _it)
    end
    ra
end

"""
select2vec(stmt::AbstractStmt, cols=1:ncols(stmt)) -> vector of rows, each row a vector

* for example, `cols=[0, 3, 4]` returns in each row vector the first, fourth and fifth columns,
  1, 4, and 5.
"""
function select2vecs(stmt, cols=1:ncols(stmt))
    ra = Vector[]
    @sync for _it in eachrow(stmt, rowvec, (cols,))
        @async push!(ra, _it)
    end
    ra
end
function select2vecs(stmt, ::Type{T}) where T
    ra = Vector{T}[]
    @sync for _it in rows(stmt, rowvec, (T,))
        @async push!(ra, _it)
    end
    ra
end
function select!(ra, stmt, ::Type{T}, start=0) where T
    @sync for (k, it) in eachrow(stmt, rowtuple, (T,))
        @async ra[start+k] = it
    end
end

function select(stmt, ::Type{T}, h; bindpar=()) where T
    reset(stmt)
    if !isempty(bindpar); bind(stmt, bindpar); end

    r = T[]
    for _it in eachrow(stmt, T, h)
        push!(r, _it)
    end
    r
end

function select(stmt, ::Type{T}, fproc::Function, iw=workers(), args...;
        kwargs=Tuple{Symbol,Any}[], bindpar=(), handover...) where T
    reset(stmt)
    if !isempty(bindpar); bind(stmt, bindpar); end

    pid = 2
    @sync for row in eachrow(stmt, T, handover)
        @async remotecall_fetch(fproc, pid, row, args...; kwargs...)
        if length(iw)>1; pid = mod(pid-1, length(iw))+2; end
    end
end

function select(db::DB; named=false,
        onlycount=false, withcount=false, bindpar=(), selargs...)::Union{Vector{NamedTuple},Vector{Tuple}}
    stmt = prepare_select(db; onlycount, withcount, selargs...)
    !isempty(bindpar) && bind(stmt, bindpar)
    # select(stmt; onlycount, withcount)
    named ? map(row, Statement(withcount ? stmt[1] : stmt)) : select(stmt; onlycount, withcount)
    # named ? map(row, Statement(stmt)) : select(stmt)
end

function select(db::DB, ::Type{T}; bindpar=(), selargs...) where T
    stmt = prepare_select(db; selargs...)
    !isempty(bindpar) && bind(stmt, bindpar)
    select(stmt, T)
end

select(db::DB, sql::AbstractString; kwargs...) = select(db; sql=sql, kwargs...)

function select(func::Function, db::DB; bindpar=(), selargs...) 
    stmt = prepare_select(db; selargs...)
    !isempty(bindpar) && bind(stmt, bindpar)
    func(stmt)
end

function select(func::Function, db::DB, ::Type{T}; bindpar=(), selargs...) where T
    stmt = prepare_select(db; selargs...)
    func(stmt, T)
end

function select2tuples(db::DB, ::Type{T}; bindpar=(), selargs...) where T
    stmt = prepare_select(db; selargs...)
    !isempty(bindpar) && bind(stmt, bindpar)
    select2tuples(stmt, T)
end

select(dbnm::AbstractString, oopts="r"; selargs...) = select(opendb(dbnm, oopts); selargs...)

function count(db::DB; bindpar=(), selargs...)
    stmt = prepare_select(db; columns="count(*)", selargs...)
    !isempty(bindpar) && bind(stmt, bindpar)
    step(stmt)
    column_int64(stmt, 0)
end

function printsep(kc::Tuple{Int, Any}, len::Int; separator='|')
    print(kc[2])
    if kc[1]<len
        print(separator)
    else
        println()
    end
end

function list(a::Array; separator='|')
    for r in eachcol(a)
        for kc in enumerate(r)
            print(kc[2])
            kc[1]<size(a, 1) ? print(separator) : println()
        end
    end
end

function list(stmt::AbstractStmt, bindpar=[]; separator='|', headers=false)
    if !isempty(bindpar)
        bind(stmt, bindpar)
    end
    if headers
        for cnm in enumerate(eachcolnm(stmt))
            print(cnm[2])
            cnm[1]<column_count(stmt) ? print(separator) : println()
        end
    end
    while step(stmt)==SQLITE_ROW
        # for kc in enumerate(eachcol(stmt))
        #     print(kc[2])
        #     kc[1]<column_count(stmt) ? print(separator) : println()
        # end
        for kc in 1:column_count(stmt)
            print(column(stmt, kc-1))
            kc<column_count(stmt) ? print(separator) : println()
        end
    end
end

list(db::DB, bindpar=[]; separator='|', headers=false, selargs...) =
list(prepare_select(db; selargs...), bindpar; separator, headers)
list(db::DB, tblname::AbstractString) = list(db; from=tblname)

function list(dbnm::AbstractString, bindpar=[]; extensions=[], kwargs...)
    opendb(dbnm, "e"; extensions) do db
        list(db, bindpar; kwargs...)
    end
end

function print(stmt::AbstractStmt)
    ncols = column_count(stmt)
    for row in stmt
        for k=1:ncols
            print(row[k])
            if k!=ncols
                print("|")
            end
        end
        println()
    end
end

include("precompile.jl")

end     # module Litesql
