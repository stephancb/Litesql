function do_precompile()
    db = opendb()
    isopen(db)
    isclosed(db)

    exec(db, "CREATE TABLE locations(id INTEGER PRIMARY KEY,"*
             " name TEXT, latitude REAL, longitude REAL);")
    table_info(db, "locations")
    Litesql.table_xinfo(db, "locations")
    x=Litesql.table_column_metadata(db, "locations", "latitude")
    Litesql.jlversion()
    exec(db, "CREATE TABLE locations_strict(id INTEGER PRIMARY KEY,"*
             " name TEXT, latitude REAL, longitude REAL) STRICT;")
    table_list(db, "locations_strict")
    insstmt = prepare_insert(db, into="locations")
    reset(insstmt)
    transaction(db)
    (nothing, "Uppsala", 49f0, 15f0) |> insstmt
    (nothing, "Tierp", 49, 15)       |> insstmt
    (nothing, "Kiruna", 67.85254648210221, 20.232396368128995) |> insstmt
    commit(db)
    transaction(db)
    insstmt = prepare_insert(db, into="locations", columns="")
    insstmt = prepare_insert(db, into="locations",
                             columns=column_names(db, "locations"; start=1, except=("name",)))

    cntstmt = prepare_select(db; from="locations", onlycount=true )
    step(cntstmt)

    selstmt,cntstmt = prepare_select(db, from="locations", where="latitude>0", withcount=true)
    n = count(cntstmt)
    
    latitudes_rad = Float64[]
    reset(selstmt)
    for _s in selstmt
        push!(latitudes_rad, Base.deg2rad(Float64(_s, 3)))
    end
    
    longitudes_rad = Float64[]
    reset(selstmt)
    for _r in rows(selstmt)
        push!(longitudes_rad, Base.deg2rad(_r[4]))
    end
    x = first(selstmt)
    x1 = Int64(selstmt, 0)
    x2 = String(selstmt, 1)
    x3 = Float64(selstmt, 2)
    x4 = Float32(selstmt, 3)

    select(db; from="locations")

    row(selstmt)
    selstmt["latitude"]
    selstmt[:latitude]
    rowtuple(selstmt)
    channel(selstmt)

    (path, io) = mktemp()
    db = opendb(path, "w")
    exec(db, "CREATE TABLE locations(id INTEGER PRIMARY KEY,"*
             " name TEXT, latitude BLOB, longitude BLOB);")
    insstmt = prepare_insert(db, into="locations")
    (1, "Uppsala", 49f0, 15f0) |> insstmt
    close!(db)
    rm(path)
end
do_precompile()
