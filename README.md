# Litesql
is a package for the [Julia Language](https://julialang.org/). It provides an interface to
the [SQLite library](http://www.sqlite.org)

This enables to handle large amounts of data (up to several 100 GB) with the performance
of Julia and the indexing power of SQLite.  

# SQLite library

The `Litesql` package uses the `Libdl.find_library` function to locate the `SQLite`
library, and it does not provide the library as an
[Artifact](https://pkgdocs.julialang.org/v1/artifacts/). 

# Open/Close database connections

    opendb(filename, flags=SQLITE_OPEN_READONLY, vfs::Ptr{Nothing}=0) -> db

opens a database file and returns a DB object with the handle to the database.
The Sqlite function [open_v2](https://www.sqlite.org/c3ref/open.html) is called.
`SQLITE_OPEN_READWRITE` and `SQLITE_OPEN_CREATE` are alternative frequently used flags.
An automatically closing AutocloseDB 'db' is returned.

    opendb(Val(:noauto), filename, flags=SQLITE_OPEN_READONLY, vfs::Ptr{Nothing}=0) -> db

returns a DB object that will not close automatically. The user needs to eventually close it with `close(db)`.

    close!(db) -> db

forces closure of the database connection. A DB object with a 'C_NULL' handle is
returned, or an 'SqliteError' is thrown. 

'do' block syntax is supported:

    opendb("mydbfile.db") do db tables(db) end

returns all tables of the database in file "mydbfile.db" with the connection automatically closed.

    opend() -> db 

returns a database living in memory.

The C style `mode` argument of `fopen` is supported:
    
    opendb(filename, "w") -> db 

# Attaching databases

    attach(db, dbnm, as) -> return_code

adds database (SQLite file) `dbnm` to the current database connection `db` under the name
`as`.

Databases can be attached in the opening function:

    opendb(filename, "a"; attachdbs=((dbnm1, as1), ...)) -> db
or
    opendb(; mode="wa", attachdbs=((dbnm1, as1),)) -> db

which read/writes to memory and reads from database file `dbnm1` with name `as1`. 

Detaching:

    detach(db, as) -> return_code, should be `SQLITE_OK`.
