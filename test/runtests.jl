using Litesql
using Libdl,Random,Test

import Base: deg2rad

struct Location
    name::String
    latitude::Float64
    longitude::Float64
end
Location(stmt) = Location(column_string(stmt, 1),
                          column_float(stmt, 2),
                          column_float(stmt, 3))

struct LocationCol
    name::String
    latitude::Float64
    longitude::Float64
    color::Char
end
LocationCol(stmt, colour) = LocationCol(column_string(stmt, 1),
                                        column_float(stmt, 2),
                                        column_float(stmt, 3),
                                        colour)
struct LatLong
    latitude::Float64
    longitude::Float64
end
LatLong(stmt) = LatLong(column_float(stmt, 0), column_float(stmt, 1))

randlat() = (rand() .- .5).*180
randlon() = (rand() .- .5).*360

@testset "type-inference" begin
    @inferred Union{SqliteDB, AutocloseDB} opendb()
    db = opendb()
    iret = exec(db,
         "CREATE TABLE locations(id INTEGER PRIMARY KEY,"*
         " name TEXT, latitude REAL, longitude REAL);")
    @inferred Union{Vector{NamedTuple},Vector{Tuple}} table_info(db, "locations")
end

function test_ll_ins(insstmt, x)
    @test SQLITE_OK==@inferred bind(insstmt, 1, x)
    @test SQLITE_DONE==@inferred step(insstmt)
    @test SQLITE_OK==@inferred reset(insstmt)
end

function test_sel_stepreset(selstmt)
    @test SQLITE_OK==reset(selstmt)
    @test SQLITE_ROW==step(selstmt)
end

@testset "Litesql.jl" begin
    # Write your tests here.
    @testset "opendb" begin
        db = opendb()
        @test isa(db, AutocloseDB)
        @test isopen(db)
        Litesql.close!(db)
        @test db.handle == C_NULL
        @test isclosed(db)
        db = opendb(":memory:", "w")
        @test isa(db, AutocloseDB)
    end
    @testset "no autoclose" begin
        db = opendb(":memory:", SqliteDB, SQLITE_OPEN_READWRITE)
        @test isa(db, SqliteDB)
        @test close(db) == SQLITE_OK
        @test_throws SqliteError close(db)
        db = opendb(Val(:noauto), ":memory:", SQLITE_OPEN_READWRITE)
        @test isa(db, SqliteDB)
        @test close(db) == SQLITE_OK
        db = opendb(":memory:", "w"; autoclose=false)
        @test isa(db, SqliteDB)
        @test close(db) == SQLITE_OK
    end
    @testset "memory allocation" begin
        p = malloc(32)
        @test p!=C_NULL
        p = realloc(p, 48)
        @test p!=C_NULL
        @test msize(p)>=48
        @test free(p)===nothing
        p = malloc(UInt64(64))
        @test p!=C_NULL
        p = realloc(p, UInt64(86))
        @test p!=C_NULL
        @test msize(p)>=86
        @test free(p)===nothing
        @test Litesql.release_memory(4)>=0
    end
    @testset "library info" begin
        @test Litesql.jlversion()>=v"3"
        @test Litesql.libversion_number()>=3000000
    end
    @testset "tables" begin
        db=opendb()
        @test exec(db,
            "CREATE TABLE locations(id INTEGER PRIMARY KEY,"*
            " name TEXT, latitude REAL, longitude REAL);")==SQLITE_OK
        x = @inferred Union{Vector{NamedTuple},Vector{Tuple}} table_info(db, "locations")
        @test (length.((x, x[1])))==(4,6)
        x = @inferred Union{Vector{NamedTuple},Vector{Tuple}} Litesql.table_xinfo(db, "locations")
        x = Litesql.table_xinfo(db, "locations")
        @test length.((x, x[1]))==(4,7)
        x = @inferred Litesql.table_column_metadata(db, "locations", "latitude")
        @test x.datatype=="REAL"
        
        columns = ["id", "name", "latitude", "longitude"]
        scol =  @inferred collect(String, Litesql.eachcolnm(db, "locations"; start=2))
        @test all(scol.==columns[3:end])
        scol =  @inferred collect(String, Litesql.eachcolnm(db, "locations"; except=("longitude",)))
        @test all(scol.==columns[1:end-1])

        @inferred Litesql.jlversion()
        if Litesql.jlversion()>=v"3.37"
            @test exec(db,
                "CREATE TABLE locations_strict(id INTEGER PRIMARY KEY,"*
                " name TEXT, latitude REAL, longitude REAL) STRICT;")==SQLITE_OK
            @test Litesql.table_list(db, "locations_strict")[:strict]==1
        end
        @test length(tables(db))>=1
        @testset "insert" begin
            sqltxt = @inferred Union{Nothing, String} sql_insert(db; into="locations")
            @test sqltxt=="INSERT INTO locations VALUES(?1,?2,?3,?4)"
            sqltxt =
                @inferred Union{Nothing, String} sql_insert(db; into="locations",
                                                            columns=column_names(db, "locations")[2:end])
            @test sqltxt=="INSERT INTO locations(name,latitude,longitude) VALUES(?1,?2,?3)"
            sqltxt = @inferred Union{Nothing, String} sql_insert(db; into="locations", conflict=:replace)
            @test sqltxt=="INSERT OR REPLACE INTO locations VALUES(?1,?2,?3,?4)"
            withstr = "loc AS (SELECT * FROM locations)"
            sqltxt = @inferred Union{Nothing, String} sql_insert(db; into="loc", with=withstr,
                                                                values="?1,?2,?3,?4")
            @test sqltxt=="WITH $withstr INSERT INTO loc VALUES(?1,?2,?3,?4)"
            sqltxt = @inferred Union{Nothing, String} sql_insert(db; into="loc", values="?1,?2,?3,?4",
                                                                 with=withstr, recursive=true)
            @test sqltxt=="WITH RECURSIVE $withstr INSERT INTO loc VALUES(?1,?2,?3,?4)"

            @test exec(db, "DELETE FROM locations")==SQLITE_OK
            insstmt = @inferred prepare_insert(db, into="locations")
            @test sql(insstmt)[1:6]=="INSERT"
            n = 2028
            @test transaction(db)==SQLITE_OK
            for k in 1:n
                reset(insstmt)
                @inbounds bind(insstmt, (nothing, randstring(vcat('A':'Z', 'a':'z')),
                                         randlat(), randlon()))
                step(insstmt)
            end
            @test commit(db)==SQLITE_OK
            cntstmt = prepare_select(db; from="locations", onlycount=true )
            @test count(cntstmt)==n

            delete(db; from="locations")
            @test count(cntstmt)==0

            transaction(db)
            ((nothing, randstring(vcat('A':'Z', 'a':'z')), randlat(), randlon()) for k in 1:n) .|> insstmt
            commit(db)
            @test count(cntstmt)==n
            
            delete(db; from="locations")
            @test count(cntstmt)==0
            insstmt = @inferred prepare_insert(db, into="locations", columns="latitude,longitude")
            ((randlat(), randlon()) for k in 1:n) .|> insstmt
            @test count(cntstmt)==n

            delete(db; from="locations")
            insstmt = @inferred prepare_insert(db, into="locations",
                                               columns=column_names(db, "locations")[2:end])
            ((randstring(vcat('A':'Z', 'a':'z')), randlat(), randlon()) for k in 1:n) .|> insstmt
            @test count(cntstmt)==n

            delete(db; from="locations")
            insstmt = @inferred prepare_insert(db, into="locations",
                                               columns=column_names(db, "locations"; start=1))
            ((randstring(vcat('A':'Z', 'a':'z')), randlat(), randlon()) for k in 1:n) .|> insstmt
            @test count(cntstmt)==n

            delete(db; from="locations")
            insstmt = @inferred prepare_insert(db, into="locations",
                                               columns=column_names(db, "locations";
                                                                    start=1, except=("name",)))
            ((randlat(), randlon()) for k in 1:n) .|> insstmt
            @test count(cntstmt)==n

            if Litesql.jlversion()>=v"3.37"
                @test exec(db, "DELETE FROM locations_strict")==SQLITE_OK
                insstmt = prepare_insert(db, into="locations_strict")
                @test sql(insstmt)[1:6]=="INSERT"
                n = 2028
                @test transaction(db)==SQLITE_OK
                for k in 1:n
                    reset(insstmt)
                    @inbounds bind(insstmt, (nothing, randstring(vcat('A':'Z', 'a':'z')),
                                             randlat(), randlon()))
                    step(insstmt)
                end
                @test commit(db)==SQLITE_OK
                cntstmt = prepare_select(db; from="locations_strict", onlycount=true )
                step(cntstmt)
                @test count(cntstmt)==n
                reset(insstmt)
                @inbounds bind(insstmt, (nothing, 1//3, randlat(), randlon()))
                @test_throws StmtError checked_step(insstmt)
                reset(insstmt)
            end
        end
        @testset "low level insert and select" begin
            ## cntstmt = @inferred prepare_select(db; from="locations", onlycount=true) # Type unstable
            cntstmt = prepare_select(db; from="locations", onlycount=true)

            delete(db; from="locations")
            @test count(cntstmt)==0

            insstmt = @inferred prepare_insert(db, into="locations",
                                               columns="latitude,longitude", values="@latitude,@longitude")
            n = 2028
            for k in 1:n
                bind(insstmt, "@latitude",  randlat())
                bind(insstmt, "@longitude", randlon())
                step(insstmt)
                reset(insstmt)
            end
            @test count(cntstmt)==n

            @test SQLITE_OK==@inferred exec(db, "CREATE TABLE ll(x)")
            insstmt = @inferred prepare_insert(db; into="ll")
            # selstmt = @inferred prepare_select(db; from="ll") # Type unstable!
            selstmt = @inferred AutoStmt prepare_select(db; from="ll")
            cntstmt = prepare_select(db; from="ll", onlycount=true)

            # Insert a NaN, retrieve as NULL (SQLite standard behaviour)
            test_ll_ins(insstmt, NaN)
            @test count(cntstmt)==1
            test_sel_stepreset(selstmt)
            @test SQLITE_NULL==column_type(selstmt, 0)
            delete(db; from="ll")
            @test count(cntstmt)==0
            
            # Insert a NaN, retrieve as NaN
            Litesql.NAN_IS_BLOB[] = true
            test_ll_ins(insstmt, NaN)
            @test count(cntstmt)==1
            test_sel_stepreset(selstmt)
            @test SQLITE_BLOB==column_type(selstmt, 0)
            @test isnan(column_float(selstmt, 0))
            delete(db; from="ll")
            @test count(cntstmt)==0
            Litesql.NAN_IS_BLOB[] = false

            # Insert all doubles as 8-byte BLOB, retriev as double
            Litesql.DOUBLE_IS_BLOB[] = true
            test_ll_ins(insstmt, 3.14)
            test_ll_ins(insstmt, NaN)
            @test count(cntstmt)==2
            test_sel_stepreset(selstmt)
            @test SQLITE_BLOB==column_type(selstmt, 0)
            @test column_float(selstmt, 0)==3.14
            @test SQLITE_ROW==step(selstmt)
            @test SQLITE_BLOB==column_type(selstmt, 0)
            @test isnan(column_float(selstmt, 0))
            delete(db; from="ll")
            @test count(cntstmt)==0
            Litesql.DOUBLE_IS_BLOB[] = false

            # Insert singles as 4-byte BLOB, retrieve as single
            Litesql.SINGLE_IS_BLOB[] = true
            test_ll_ins(insstmt, Float32(3.14))
            test_ll_ins(insstmt, NaN32)
            @test count(cntstmt)==2
            test_sel_stepreset(selstmt)
            @test SQLITE_BLOB==column_type(selstmt, 0)
            @test column_float32(selstmt, 0)==3.14f0
            @test column_float(selstmt, 0, Float32)==3.14f0
            @test SQLITE_ROW==step(selstmt)
            @test isnan(column_float32(selstmt, 0))
            @test isnan(column_float(selstmt, 0, Float32))
            delete(db; from="ll")
            @test count(cntstmt)==0
            Litesql.SINGLE_IS_BLOB[] = false

            # Insert UInt64 as signed long, retrieve as UInt64
            Litesql.SINGLE_IS_BLOB[] = true
            x = UInt64(2)^64-1
            test_ll_ins(insstmt, x)
            @test count(cntstmt)==1
            test_sel_stepreset(selstmt)
            @test SQLITE_INTEGER==column_type(selstmt, 0)
            @test column_uint64(selstmt, 0)==x
            delete(db; from="ll")
            @test count(cntstmt)==0

            # Insert Int8, retrieve Int8
            x_int8 = Int8(2^7-1)
            test_ll_ins(insstmt, x_int8)
            x_uint8 = UInt8(2^8-1)
            test_ll_ins(insstmt, x_uint8)
            @test count(cntstmt)==2
            test_sel_stepreset(selstmt)
            @test SQLITE_INTEGER==column_type(selstmt, 0)
            @test column_int(selstmt, 0, Int8)==x_int8
            @test SQLITE_ROW==step(selstmt)
            @test SQLITE_INTEGER==column_type(selstmt, 0)
            @test column_int(selstmt, 0, UInt8)==x_uint8
            delete(db; from="ll")
            @test count(cntstmt)==0

            # Insert Int16, retrieve Int16
            x_int16 = Int16(2^15-1)
            test_ll_ins(insstmt, x_int16)
            x_uint16 = UInt16(2^16-1)
            test_ll_ins(insstmt, x_uint16)
            @test count(cntstmt)==2
            test_sel_stepreset(selstmt)
            @test SQLITE_INTEGER==column_type(selstmt, 0)
            @test column_int(selstmt, 0, Int16)==x_int16
            @test SQLITE_ROW==step(selstmt)
            @test SQLITE_INTEGER==column_type(selstmt, 0)
            @inferred column_int(selstmt, 0, UInt16)
            @test column_int(selstmt, 0, UInt16)==x_uint16
            delete(db; from="ll")
            @test count(cntstmt)==0

            test_ll_ins(insstmt, nothing)
            test_ll_ins(insstmt, missing)
            @test count(cntstmt)==2
            test_sel_stepreset(selstmt)
            @test SQLITE_NULL==column_type(selstmt, 0)
            @inferred Litesql.column_null(selstmt, 0)
            @test ismissing(Litesql.column_null(selstmt, 0))
            @test SQLITE_ROW==step(selstmt)
            @test SQLITE_NULL==column_type(selstmt, 0)
            @inferred Litesql.column_null(selstmt, 0)
            @test ismissing(Litesql.column_null(selstmt, 0))
            delete(db; from="ll")
            @test count(cntstmt)==0

            # Insert from an IOBuffer, retrieve back into another IOBuffer
            io_in = IOBuffer()
            write(io_in, "Hallo SQLite")
            test_ll_ins(insstmt, io_in)
            @test count(cntstmt)==1
            test_sel_stepreset(selstmt)
            @test SQLITE_BLOB==column_type(selstmt, 0)
            io_out = IOBuffer()
            @test io_in.size==column_blob(selstmt, 0, io_out)
            @test all(take!(io_out) .== take!(io_in))
            delete(db; from="ll")
            @test count(cntstmt)==0

            # Insert π (irrational) as a Julia serialized BLOB, retrieve 
            x_pi = π
            test_ll_ins(insstmt, x_pi)
            x_cmplx = ComplexF64(√(2), -1)
            test_ll_ins(insstmt, x_cmplx)
            @test count(cntstmt)==2
            test_sel_stepreset(selstmt)
            @test SQLITE_BLOB==column_type(selstmt, 0)
            @test column_object(selstmt, 0)==x_pi
            @test SQLITE_ROW==step(selstmt)
            @test SQLITE_BLOB==column_type(selstmt, 0)
            @test column_object(selstmt, 0)==x_cmplx
            delete(db; from="ll")
            @test count(cntstmt)==0

        end

        @testset "update" begin
            updstmt = @inferred prepare_update(db, table="locations", columns="name")
            bind(updstmt, "unknown")
            @test step(updstmt)==SQLITE_DONE
            updstmt = @inferred prepare_update(db, table="locations", columns=["name"])
            bind(updstmt, "still unknown")
            @test step(updstmt)==SQLITE_DONE
            updstmt = @inferred prepare_update(db, table="locations", set="name='unbekannt'")
            @test step(updstmt)==SQLITE_DONE
        end
        @testset "low level select" begin
            insstmt = @inferred prepare_insert(db; into="ll")
            test_ll_ins(insstmt, nothing)

            selstmt = @inferred AutoStmt prepare_select(db; from="ll")
            @test 1==@inferred ncols(selstmt)
            @test SQLITE_ROW==@inferred step(selstmt)
            @test SQLITE_NULL==@inferred column_type(selstmt, 0)
            @test "x"==@inferred column_name(selstmt, 0)
            @test 0==@inferred column_double(selstmt, 0)
            @test_throws BoundsError column_double(selstmt, 1)
            @test 0==@inferred column_int(selstmt, 0)
            @test 0==@inferred column_int64(selstmt, 0)
            @test 0==@inferred column_uint64(selstmt, 0)
            @test Ptr{Cvoid}(0)==@inferred column_text(selstmt, 0)
            @test Ptr{Cvoid}(0)==@inferred column_blob(selstmt, 0)

            @test NaN===@inferred Union{Float64, SqliteError} column_float(selstmt, 0)
            @test missing===@inferred Missing column_string(selstmt, 0)
            @test missing===@inferred Missing nullable(selstmt, Float64, 0)
            delete(db; from="ll")
            @test SQLITE_OK==reset(insstmt)
            @test_throws BoundsError bind(insstmt, 2, 3.14)
            @test SQLITE_OK==bind(insstmt, 1, 3.14)
            @test SQLITE_DONE==step(insstmt)
            @test SQLITE_OK==reset(selstmt)
            @test SQLITE_ROW==@inferred step(selstmt)
            @test SQLITE_FLOAT==@inferred column_type(selstmt, 0)
            @test 3.14==column_double(selstmt, 0)
            @test 3.14==column(selstmt, Float64, 0)
            @test 3.14==column_float(selstmt, 0)
            @test 3.14==nullable(selstmt, Float64, 0)
            @test 3.14==column(selstmt, Float64, "x")
            @test 3==column_int(selstmt, 0)
            @test 3==column_int64(selstmt, 0)
            @test 3==column(selstmt, Int, 0)
            @test "3.14"==column_string(selstmt, 0)

        end
        @testset "select" begin
            selstmt,cntstmt = prepare_select(db, from="locations", where="latitude>0", withcount=true)
            n = count(cntstmt)
            @test n>0
            names = String[]
            latitudes = Float64[]
            longitude = Float64[]
            while step(selstmt)==SQLITE_ROW
                @inbounds push!(names, column_string(selstmt, 1))
                @inbounds push!(latitudes, column_float(selstmt, 2))
                @inbounds push!(longitude, column_float(selstmt, 3))
            end
            @test length(names)==length(latitudes)==length(longitude)==n

            reset(selstmt)
            step(selstmt)
            @test_throws BoundsError column_float(selstmt, 4)
            # This should actually NOT throw a BoundsError?
            @test_throws BoundsError @inbounds column_double(selstmt, 5)
            # @test @inbounds column_double(selstmt, 5)==0

            reset(selstmt)
            step(selstmt)
            @inferred Missing column_string(selstmt, 1)
            @inferred Union{Float64, SqliteError} column_float(selstmt, 2)
            @test length(latitudes)==n
            reset(selstmt)
            step(selstmt)
            @inferred @NamedTuple{id::Int64, name::String, latitude::Float64, longitude::Float64} row(selstmt)

            reset(selstmt)
            names = String[]
            latitudes = Float64[]
            longitude = Float64[]
            while step(selstmt)==SQLITE_ROW
                @inbounds push!(names, String(selstmt, 1))
                @inbounds push!(latitudes, Float64(selstmt, 2))
                @inbounds push!(longitude, Float64(selstmt, 3))
            end
            @test length(names)==n

            names = String[]
            latitudes = Float64[]
            longitude = Float64[]
            reset(selstmt)
            while step(selstmt)==SQLITE_ROW
                push!(names, column_string(selstmt, "name"))
                push!(latitudes, column_float(selstmt, "latitude"))
                push!(longitude, column_float(selstmt, "longitude"))
            end
            @test length(latitudes)==n

            # explicitely step through the selstmt:
            names = String[]
            latitudes = Float64[]
            longitude = Float64[]
            reset(selstmt)
            while step(selstmt)==SQLITE_ROW
                push!(names, column_string(selstmt, :name))
                push!(latitudes, column_float(selstmt, :latitude))
                push!(longitude, column_float(selstmt, :longitude))
            end
            @test length(latitudes)==n
            chklat = sum(latitudes)
            chklon = sum(longitude)

            reset(selstmt)
            a  = Vector{Vector{Any}}(undef, n)
            id = Vector{Int64}(undef, n)
            k = 1
            while step(selstmt)==SQLITE_ROW
                a[k]  = Vector{Any}(selstmt)
                id[k] = Int64(selstmt)
                k   += 1
            end
            # @test [a[k][1] for k in 1:length(a)].==1:n
            # @test [x[1] for x in a] .== 1:n
            @test all([x[1] for x in a] .== id)

            @test all(first(selstmt) .== (column_int(selstmt, 0), column_string(selstmt, 1),
                                          column_float(selstmt, 2), column_float(selstmt, 3)))

            @testset "iterators" begin
                # iterate the selstmt:
                latitudes = Float64[]
                reset(selstmt)
                for _s in selstmt
                    push!(latitudes, column_float(_s, :latitude))
                end
                @test length(latitudes)==n
                @test sum(latitudes)==chklat

                # iterate over the rows of the selstmt:
                longitudes = Float64[]
                reset(selstmt)
                for _l in rows(selstmt)
                    push!(longitudes, _l[4])
                end
                @test length(longitudes)==n
                @test sum(longitudes)==chklon

                # iterate over the rows of the selstmt:
                locs = Location[]
                reset(selstmt)
                for _l in rows(selstmt, Location)
                    push!(locs, _l)
                end
                @test length(locs)==n
                @test sum(x -> x.latitude,  locs)==chklat
                @test sum(x -> x.longitude, locs)==chklon

                # iterate with handover:
                locs = LocationCol[]
                reset(selstmt)
                for _l in rows(selstmt, LocationCol, 'k')
                    push!(locs, _l)
                end
                @test length(locs)==n
                @test locs[1].color=='k'
                @test sum(x -> x.latitude,  locs)==chklat
                @test sum(x -> x.longitude, locs)==chklon

                # apply function in iteration:
                latlongs_rad = NTuple{2, Float64}[]
                reset(selstmt)
                deg2rad(stmt::AbstractStmt) = Base.deg2rad.((column_float(stmt, 2),column_float(stmt, 3)))
                for _r in rows(selstmt, deg2rad)
                    push!(latlongs_rad, _r)
                end
                @test length(latlongs_rad)==n
                @test sum(x -> x[1],  latlongs_rad)≈Base.deg2rad(chklat)
                @test sum(x -> x[2],  latlongs_rad)≈Base.deg2rad(chklon)

                # apply function in do block iteration
                latlongs_rad = NTuple{2, Float64}[]
                reset(selstmt)
                foreach(selstmt) do _s
                    push!(latlongs_rad, deg2rad(_s))
                end
                @test length(latlongs_rad)==n
                @test sum(x -> x[1],  latlongs_rad)≈Base.deg2rad(chklat)
                @test sum(x -> x[2],  latlongs_rad)≈Base.deg2rad(chklon)

            end

            names = String[]
            latitudes = Float64[]
            longitude = Float64[]
            reset(selstmt)
            statement = Statement(selstmt)
            while step(statement)==SQLITE_ROW
                push!(names, column_string(statement, "name"))
                push!(latitudes, column_float(statement, "latitude"))
                push!(longitude, column_float(statement, "longitude"))
            end
            @test length(latitudes)==n

            @test SQLITE_OK==reset(selstmt)
            @test SQLITE_ROW==step(selstmt)
            # t = @inferred selstmt[1]
            @test selstmt[1]==names[1]
            @test selstmt["latitude"]==latitudes[1]
            @test selstmt[:longitude]==longitude[1]
            t = row(selstmt)
            @test t[:name]==names[1]
            @test t[:latitude]==latitudes[1]
            @test t.longitude==longitude[1]

            @test (nm = @inferred column_name(selstmt, 2))=="latitude"
            @test 2==Litesql.colindex(selstmt, "latitude")

            ll = @inferred Union{Vector{NamedTuple},Vector{Tuple}} select(db; from="locations", where="latitude>0")
            @test length(ll)==n
            @test ll[1][2]==names[1]
            @test ll[1][3]==latitudes[1]
            @test ll[1][4]==longitude[1]

            ll = @inferred Union{Vector{NamedTuple},Vector{Tuple}} select(db, LatLong;
                                  from="locations", columns="latitude,longitude", where="latitude>0")
            @test length(ll)==n
            @test eltype(ll)==LatLong
            @test ll[1].latitude==latitudes[1]

            ll = @inferred select2tuples(db, Float32;
                               from="locations", columns="latitude,longitude", where="latitude>0")

            reset(selstmt)
            llc = @inferred collect(channel(selstmt))
            @test length(llc)==n
            @test eltype(llc)==Tuple
            @test llc[1][3]==latitudes[1]

            reset(selstmt)
            @inferred collect(channel(selstmt, Location))
            llc = collect(channel(selstmt, Location))
            @test length(llc)==n
            @test eltype(llc)==Location
            @test llc[1].latitude==latitudes[1]
        end

        # test vector extension
        @testset "vectors" begin
            @test SQLITE_OK==@inferred exec(db, "CREATE TABLE vec(x);")
            pi32 = Float32(π)
            Litesql.SINGLE_IS_BLOB[] = true
            insstmt = prepare_insert(db; into="vec")
            @test SQLITE_OK==bind(insstmt, 1, pi32)
            @test SQLITE_DONE==step(insstmt)
            @test SQLITE_OK==reset(insstmt)
            selstmt = prepare_select(db; from="vec", columns="x")
            @test SQLITE_ROW==step(selstmt) 
            @test pi32==column_float32(selstmt, 0)
            delete(db; from="vec")
            v32 = rand(Float32, 5)
            @test SQLITE_OK==bind(insstmt, 1, v32)
            @test SQLITE_OK==reset(insstmt)
            @test SQLITE_DONE==step(insstmt)
            @test SQLITE_OK==reset(insstmt)
            @test SQLITE_OK==reset(selstmt)
            @test SQLITE_ROW==step(selstmt) 
            @test all(v32.==column_vector(selstmt, 5, Float32, 0))
            delete(db; from="vec")
            # ll = select(db, LatLong; from="locations", where="latitude>0")
            # @show typeof(ll)
            # @show size(ll)
            # @show ll[1]
            # @test SQLITE_OK==bind(insstmt, 1, ll)
            # @test SQLITE_DONE==step(insstmt)
            # @test SQLITE_OK==reset(insstmt)
            # @test SQLITE_OK==reset(selstmt)
            # @test SQLITE_ROW==step(selstmt) 
            # @shoe column_vector(selstmt, 5, BigFloat, 0)[1]
            # @test all(ll.==column_vector(selstmt, 5, Tuple{Int, String, Float64, Float64}, 0))
            # b32 = rand(BigFloat, 5)
            # @test SQLITE_OK==bind(insstmt, 1, b32)
            # @test SQLITE_DONE==step(insstmt)
            # @test SQLITE_OK==reset(insstmt)
            # @test SQLITE_OK==reset(selstmt)
            # @test SQLITE_ROW==step(selstmt) 
            # @test all(b32.==column_vector(selstmt, 5, BigFloat, 0))

        end
        # n = count(db; from="sqlite_stmt")
        # @show n
        # list(db; from="sqlite_stmt")
        # close!(db)
    end
end
