module ZippedTrace

# using Litesql,.ZipFile
using Litesql,ZipFile
import Litesql.@s3

function zipsql(zf_::Ptr{ZipFile.WritableFile}, sqltxt::Ptr{UInt8})
    zf = unsafe_pointer_to_objref(zf_)::ZipFile.WritableFile
    write(zf, unsafe_string(sqltxt)*"\n")::Int
end

const zipsql_c = @cfunction(zipsql, Int, (Ptr{ZipFile.WritableFile}, Ptr{UInt8}))

trace_v2(db::DB, umask, cb_c, ctxp) =
    ccall((@s3 trace_v2), Cint, (Ptr{Cvoid},Cuint,Ptr{Cvoid},Ptr{Cvoid}),
          db.handle, Cuint(umask), cb_c, ctxp)
trace(db::DB) =
    ccall((@s3 trace),
          Ptr{Cvoid}, (Ptr{Cvoid}, Ptr{Cvoid}, Ptr{Cvoid}),
          db.handle, printsql_c, C_NULL)

trace(db::DB, io::IOStream) =
    ccall((@s3 trace),
          Ptr{Cvoid}, (Ptr{Cvoid}, Ptr{Cvoid}, Ptr{Cvoid}),
          db.handle, printfsql_c, pointer_from_objref(io))

trace(db::DB, zf::ZipFile.WritableFile) =
    ccall((@s3 trace),
          Ptr{Cvoid}, (Ptr{Cvoid}, Ptr{Cvoid}, Ptr{Cvoid}),
          db.handle, zipsql_c, pointer_from_objref(zf))

function zipsqlprof(zf_::Ptr{ZipFile.WritableFile}, sqltxt::Ptr{UInt8}, nanosec::UInt64)
    zf = unsafe_load(zf_)::ZipFile.WritableFile
    write(zf, "$(unsafe_string(sqltxt)) -- $(nanosec/1e6) msec\n")::Int
end

const zipsqlprof_c   = @cfunction(zipsqlprof, Int, (Ptr{ZipFile.WritableFile}, Ptr{UInt8}, Culong))

profile(db::DB,  zf::Ptr{ZipFile.WritableFile}) =
    ccall((@s3 profile),
          Ptr{Cvoid}, (Ptr{Cvoid}, Ptr{Cvoid}, Ptr{Cvoid}),
          db.handle, zipsqlprof_c, pointer_from_objref(zf))

end # module ZippedTrace
