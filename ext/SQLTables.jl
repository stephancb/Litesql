module SQLTables

using Litesql,Tables

Tables.istable(::Type{<:AbstractStmt}) = true	
Tables.rowaccess(::Type{<:AbstractStmt}) = true
Tables.rows(stmt::AbstractStmt) = stmt
Tables.getcolumn(stmt::AbstractStmt, i::Int) = column(stmt, i-1)
Tables.getcolumn(stmt::AbstractStmt, nm::Symbol) = column(stmt, nm)
Tables.columnnames(stmt::AbstractStmt) = [column_name(stmt, k) for k in 0:ncols(stmt)-1]

end # module SQLTables
